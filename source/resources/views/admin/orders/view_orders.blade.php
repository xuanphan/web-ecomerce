@extends('layouts.adminLayout.admin_design')

@section('content')
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{url('/admin/dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Orders</a> <a href="#" class="current">View Orders</a> </div>
    <h1>Orders</h1> 
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Orders Table</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Order ID</th>
                  <th>Order Date</th>
                  <th>Customer Name</th>
                  <th>Customer Email</th>
                  <th>Ordered Products</th>
                  <th>Grand Total</th>
                  <th>Order Status</th>
                  <th>Payment Method</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              	@foreach($orders as $order)
                <tr class="gradeX">
                  
                  <td>{{ $order->id }}</td>
                  <td> {{ date("H:i:s - d/m/Y", strtotime($order->created_at)) }}</td>
                  <td>{{ $order->name }}</td>
                  <td>{{ $order->user_email }}</td>
                  <td>
                     @foreach($order->orders as $pro)
                        {{ $pro->product_code}} (x{{$pro->qty}}) <br/>
                    @endforeach
                  </td>
                  <td>{{ number_format($order->grand_total)}} VNĐ</td>
                  <td>@if($order->order_status==1) 
                      Đã duyệt đơn
                      @elseif($order->order_status==2)
                      Đang giao hàng
                      @elseif($order->order_status==3)
                      Đã giao hàng
                       @elseif($order->order_status==4)
                      <a class="cancel" id="{{ $order->id }}" href="#cancel" data-toggle="modal">Đã huỷ đơn</a>
                      @else
                      Chưa xử lý
                   @endif</td>
                  <td>{{ $order->payment_method }}</td>
                  <td><a target="_blank" href="{{url('admin/view-order/'.$order->id)}}" class="btn btn-success btn-mini">View Order Details</a> </td>
                </tr>
               @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="cancel" class="modal hide">
  <div class="modal-header">
    <button data-dismiss="modal" class="close" type="button">×</button>
    <h3>Đơn bị huỷ</h3>
  </div>
  <div class="modal-body">
    <p id="p1"></p>
    <p id="p2"></p>
    <p id="p3"></p>
    <p id="p4"></p>
    <p id="p5"></p>
  </div>
</div>
@endsection