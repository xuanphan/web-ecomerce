@extends('layouts.adminLayout.admin_design')

@section('content')
<!--main-container-part-->
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Order</a> </div>
    <h1>Order - {{$ordersDetails->id}}</h1>
    @if(Session::has('flash_message_error'))
                
        <div class="alert alert-error alert-block">

            <button type="button" class="close" data-dismiss="alert">×</button> 

            <strong>{!!session('flash_message_error') !!}</strong>

        </div>
    @endif 
    @if(Session::has('flash_message_success'))
    
        <div class="alert alert-success alert-block">

            <button type="button" class="close" data-dismiss="alert">×</button> 

            <strong>{!!session('flash_message_success') !!}</strong>

        </div>
    @endif
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span6">
         <div class="widget-box">
            <div class="widget-title">
              <h5>Order Details</h5>
            </div>
            <div class="widget-content nopadding">
              <table class="table table-striped table-bordered">
                
                <tbody>
                  <tr>
                    <td class="taskDesc">Order Date</td>
                    <td class="taskStatus"><span class="in-progress">{{ date("H:i:s - d/m/Y", strtotime($ordersDetails->created_at)) }}</span></td>
                  </tr>
                  <tr>
                    <td class="taskDesc">Order Status</td>
                    <td class="taskStatus"><span class="pending">{{ $ordersDetails->order_status }}</span></td>
                  </tr>
                  <tr>
                    <td class="taskDesc">Order Total</td>
                    <td class="taskStatus"><span class="pending">{{ number_format($ordersDetails->grand_total) }} VNĐ</span></td>
                  </tr>
                  <tr>
                    <td class="taskDesc">Shipping Charges</td>
                    <td class="taskStatus"><span class="pending">@if(!empty($ordersDetails->shipping_charges)){{ $ordersDetails->shipping_charges }}@else Free @endif</span></td>
                  </tr>
                  <tr>
                    <td class="taskDesc">Coupoun Code</td>
                    <td class="taskStatus"><span class="pending">{{ $ordersDetails->coupon_code }}</span></td>
                  </tr>
                  <tr>
                    <td class="taskDesc">Coupoun Amount</td>
                    <td class="taskStatus"><span class="pending">{{ number_format($ordersDetails->coupon_amount) }} VNĐ</span></td>
                  </tr>
                  <tr>
                    <td class="taskDesc">Payment Method</td>
                    <td class="taskStatus"><span class="pending">{{ $ordersDetails->payment_method }}</span></td>
                  </tr>
                </tbody>
              </table>
          </div>
        </div>
        <div class="accordion" id="collapse-group">
          <div class="accordion-group widget-box">
            <div class="accordion-heading">
              <div class="widget-title">
                <h5>Billing Details</h5>
              </div>
            </div>
            <div class="collapse in accordion-body" id="collapseGOne">
              <div class="widget-content"> 
                <strong>Họ và tên: </strong>  {{$userDetails->name}} <br>
                <strong>Ngày sinh: </strong>  @if(!empty($userDetails->birthday)){{date('d/m/Y',strtotime($userDetails->birthday))}}@else <span class="label label-default">Chưa cập nhật</span> @endif <br>
                <strong>Giới tính: </strong>  @if($userDetails->sex == 1)Nam @elseif($userDetails->sex == 2) Nữ @elseif($userDetails->sex == 3) Khác @else <span class="label label-default">Chưa cập nhật</span> @endif <br>
                <strong>E-mail: </strong>  @if(!empty($userDetails->email)){{$userDetails->email}}@else <span class="label label-default">Chưa cập nhật</span> @endif <br>
                <strong>Số điện thoại:</strong>  @if(!empty($userDetails->phone)){{$userDetails->phone}}@else <span class="label label-default">Chưa cập nhật</span> @endif <br>
                <strong>Địa chỉ: </strong>  {{$userDetails->address}} <br>

               </div>
            </div>
          </div>
        </div>
      </div>
      <div class="span6">
        <div class="widget-box">
          <div class="widget-title">
            <h5>Customer Details</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-striped table-bordered">
              
              <tbody>
                <tr>
                  <td class="taskDesc">Customer Name</td>
                  <td class="taskStatus"><span class="in-progress">{{ $ordersDetails->name }}</span></td>
                </tr>
                <tr>
                  <td class="taskDesc">Customer E-mail</td>
                  <td class="taskStatus"><span class="pending">{{ $ordersDetails->user_email }}</span></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="accordion" id="collapse-group">
          <div class="accordion-group widget-box">
            <div class="accordion-heading">
              <div class="widget-title">
                <h5>Shipping Details</h5>
              </div>
            </div>
            <div class="collapse in accordion-body" id="collapseGOne">
              <div class="widget-content">
                <strong>Họ và tên: </strong>  {{ $ordersDetails->name}} <br>
                <strong>E-mail liên hệ: </strong>  @if(!empty( $ordersDetails->user_email)){{ $ordersDetails->user_email}}@else <span class="label label-default">Chưa cập nhật</span> @endif <br>
                <strong>Số điện thoại liên hệ: </strong>  @if(!empty( $ordersDetails->phone)){{ $ordersDetails->phone}}@else <span class="label label-default">Chưa cập nhật</span> @endif <br>
                <strong>Địa chỉ nhận hàng: </strong>{{ $ordersDetails->address}} <br>
              </div>
            </div>
          </div>
        </div>
        <div class="accordion" id="collapse-group">
          <div class="accordion-group widget-box">
            <div class="accordion-heading">
              <div class="widget-title">
                <h5>Update Order Status</h5>
              </div>
            </div>
            <div class="collapse in accordion-body" id="collapseGOne">
              <div class="widget-content">
                <form action="{{url('admin/update-order-status')}}" method="post" class="form-horizontal">{{csrf_field()}}
                  <div class="control-group">
                    <div class="control-group">
                      <label class="control-label">Cập nhật đơn hàng:</label>
                      <div class="controls">
                        <input type="hidden" name="order_id" value="{{$ordersDetails->id}}">
                        <input type="hidden" name="order_email" value="{{$ordersDetails->user_email}}">
                        @if($ordersDetails->order_status==0)
                        <label>
                          <input type="radio" name="Status" value="1" checked  onclick="selectStatus1();"/>
                          Đã duyệt đơn</label>
                        <label>
                          <input type="radio" name="Status" value="2"  onclick="selectStatus2();" />
                          Đang giao hàng</label>
                        <label>
                          <input type="radio" name="Status" value="3"  onclick="selectStatus3();" />
                          Đã giao hàng</label>
                        <label>
                          <input type="radio" name="Status" value="4" onclick="selectStatus4();" />
                          Đã huỷ đơn</label>
                        @else
                        <label>
                          <input type="radio" name="Status" value="1" @if($ordersDetails->order_status==1) checked @endif onclick="selectStatus1();"/>
                          Đã duyệt đơn</label>
                        <label>
                          <input type="radio" name="Status" value="2"  @if($ordersDetails->order_status==2) checked @endif onclick="selectStatus2();" />
                          Đang giao hàng</label>
                        <label>
                          <input type="radio" name="Status" value="3"  @if($ordersDetails->order_status==3) checked @endif onclick="selectStatus3();" />
                          Đã giao hàng</label>
                        <label>
                          <input type="radio" name="Status" value="4"  @if($ordersDetails->order_status==4) checked @endif onclick="selectStatus4();" />
                          Đã huỷ đơn</label>
                        @endif
                      </div>
                    </div>
                    <label class="control-label">Lý do huỷ:</label>
                    <div class="controls">
                      <textarea type="text" name="reason" id="reson"  @if($ordersDetails->order_status !=4) disabled @endif ></textarea>
                    </div>
                  </div>
                  <div class="form-actions">
                    <button type="submit" class="btn btn-info pull-right">Cập nhật</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row-fluid">
      <table id="example" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th><center>Mã sản phẩm</center></th>
                <th><center>Tên sản phẩm</center></th>
                <th><center>Màu sắc</center></th>
                <th><center>Đơn giá</center></th>
                <th><center>Số lượng đặt</center></th>
            </tr>
        </thead>
        <tbody>
            @foreach($ordersDetails->orders as $pro)
            <tr>
                <td>{{ $pro->product_code }}</td>
                <td>{{ $pro->product_name }}</td>
                <td>{{ $pro->product_color }}</td>
                <td>{{ number_format($pro->price) }} VNĐ</td>
                <td>{{ $pro->qty }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @if($ordersDetails->order_status == 1 || $ordersDetails->order_status == 2 || $ordersDetails->order_status == 3)
    <div class="pull-right">
      <a href="{{url('/get-order-detail/'.$ordersDetails->id)}}"><button type="button" class="btn btn-info">Xuất Hoá Đơn</button></a>  
    </div>
    @endif
  </div>
  </div>
</div>
@endsection