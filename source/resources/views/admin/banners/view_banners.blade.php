@extends('layouts.adminLayout.admin_design')

@section('content')
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{url('/admin/dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Banners</a> <a href="#" class="current">View Banners</a> </div>
    <h1>Banners</h1>
    @if(Session::has('flash_message_error'))
                
                    <div class="alert alert-error alert-block">

                        <button type="button" class="close" data-dismiss="alert">×</button> 

                        <strong>{!!session('flash_message_error') !!}</strong>

                    </div>
                @endif 
                @if(Session::has('flash_message_success'))
                
                    <div class="alert alert-success alert-block">

                        <button type="button" class="close" data-dismiss="alert">×</button> 

                        <strong>{!!session('flash_message_success') !!}</strong>

                    </div>
                @endif 
  </div>
  <div class="container-fluid">
    
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> 
            <span class="icon"><i class="icon-th"></i></span>
            <h5>Banners Table</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Banner ID</th>
                  <th>Title</th>
                  <th>Type</th>
                  <th>Link</th>
                  <th>Images</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              	@foreach($banners as $banner)
                <tr class="gradeX">
                  <td>{{ $banner->id }}</td>
                  <td>{{ $banner->title }}</td>
                  <td>
                    @if($banner->type == 1)
                      Slide
                    @elseif($banner->type == 2)
                      Banner
                    @else
                      Partner
                    @endif
                  </td>
                  <td>{{ $banner->link }}</td>
                  <td>
                    @if(!empty($banner->image))
                      <img src="{{ asset('/images/frontend_images/banners/'.$banner->image)}}" style="width: 100px">
                    @endif
                  </td>
                  <td>
                    @if($banner->status == 1)
                      Đang Hoạt Động
                    @else
                      Đã tắt
                    @endif
                  </td>
                  <td class="center"> 
                    <a href="#myModal{{ $banner->id }}" data-toggle="modal" class="btn btn-info btn-mini">Detail & Edit</a>
                    @if($banner->type == 1)
                      @if($bannerslide > 2 || $bannerStatus > 2)
                          <a rel="{{$banner->id}}" rel1="delete-banner" href="javascript:" class="btn btn-danger btn-mini deleteRecord">Delete</a>
                      @endif
                    @else
                      <a rel="{{$banner->id}}" rel1="delete-banner" href="javascript:" class="btn btn-danger btn-mini deleteRecord">Delete</a>
                    @endif
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
            @foreach($banners as $banner)
            <div id="myModal{{ $banner->id }}" class="modal hide">
              <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">×</button>
                <h3>Slide Banner</h3>
              </div>
              <div class="modal-body">
                <div class="widget-content nopadding">
                  <form enctype="multipart/form-data" class="row-fluid" method="post" action="{{ url('admin/update-banner/')}}">{{csrf_field() }}
                    <div class="row-fluid">
                      @if(!empty($banner->image))
                        <img src="{{ asset('/images/frontend_images/banners/'.$banner->image)}}" style="max-width: 270px; max-height: 257px;">
                      @endif
                        <div class="pull-right">
                          <div class="control-group">
                            <label><strong>BannerID:</strong> {{$banner->id}}</label>
                            <input type="hidden" name="banner-id" value="{{$banner->id}}">
                          </div>
                          <div class="control-group">
                            <label><strong>Type banner:</strong> 

                              @if($banner->type == 1)
                                Slide
                              @elseif($banner->type == 2)
                                Banner
                              @else
                                Partner
                              @endif
                            </label>
                            <input type="hidden" name="type_banner" value="{{$banner->type}}">
                          </div>
                          <div class="control-group">
                            <label class="control-label">Title:</label> 
                            <div class="controls">
                              <input class="form-control" name="title" value="{{ $banner->title }}">
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label">Link:</label>
                            <div class="controls">
                              <input name="link" value="{{ $banner->link }}">
                            </div>
                          </div>
                          <div class="control-group">
                            <input type="hidden" name="current_image" value="{{$banner->image}}">
                            <input type="file" name="image" id="image">
                          </div>
                          <div class="control-group">
                            <label class="control-label">Show
                                <input type="checkbox" name="status" value="1" @if($banner->status == 1) checked @endif>
                            </label>
                          </div>
                          <div class="controls">
                            <input type="submit" value="Update" class="btn btn-success">
                          </div>
                        </div>  
                      </div>  
                  </form>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection