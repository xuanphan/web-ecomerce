@extends('layouts.adminLayout.admin_design')
@section('content')

<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{url('/admin/dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->
  <div class="container-fluid">
    <div class="quick-actions_homepage">
      <ul class="quick-actions">
        <li class="bg_lg span3"> <a href=""> <i class="icon-signal"></i> Tổng sản phẩm</a> </li>
        <li class="bg_ly"> <a href=""> <i class="icon-inbox"></i> Số đơn hàng </a> </li>
        <li class="bg_lb"> <a href=""><i class="icon-th"></i>Kho</a> </li>
        <li class="bg_lo span3"> <a href=""> <i class="icon-th-list"></i> Doanh số</a> </li>
      </ul>
    </div>
<!--End-Action boxes-->    

<!--Chart-box-->    
    <div class="row-fluid">
      <div class="widget-box">
        <div class="widget-title bg_lg"><span class="icon"><i class="icon-signal"></i></span>
          <h5>Bảng Thống Kê</h5>
        </div>
        <div class="widget-content" >
          <div class="row-fluid">
            <div class="span9">
              <div class="chart"></div>
            </div>
            <div class="span3">

            </div>
          </div>
        </div>
      </div>
    </div>
<!--End-Chart-box--> 
    
  </div>
</div>

<!--end-main-container-part-->
@endsection