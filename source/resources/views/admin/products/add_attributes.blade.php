@extends('layouts.adminLayout.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{url('/admin/dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Products</a> <a href="#" class="current">Add Product Attribute</a> </div>
    <h1>Product Attribute</h1>
    @if(Session::has('flash_message_error'))       
        <div class="alert alert-error alert-block">

            <button type="button" class="close" data-dismiss="alert">×</button> 

            <strong>{!!session('flash_message_error') !!}</strong>

        </div>
    @endif 
    @if(Session::has('flash_message_success'))
    
        <div class="alert alert-success alert-block">

            <button type="button" class="close" data-dismiss="alert">×</button> 

            <strong>{!!session('flash_message_success') !!}</strong>

        </div>
    @endif 
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Form validation</h5>
          </div>
          <div class="widget-content nopadding">
            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{('/admin/add-attribute/'.$productDetails->id)}}" name="add_attribute" id="add_attribute">{{csrf_field() }}
              <input type="hidden" name="product_id" value="{{$productDetails->id}}"/>
              <div class="control-group">
                <label class="control-label">Product Name</label>
                <label class="control-label"><b>{{$productDetails->product_name}}</b></label>
              </div>
              <div class="control-group">
                <label class="control-label">Product Code</label>
                <label class="control-label"><b>{{$productDetails->product_code}}</b></label>
              </div>
              <div class="control-group">
                <label class="control-label"></label>
                <div class="field_wrapper">
                    <div>
                        <input required type="text" name="sku[]" id="sku" placeholder="SKU" style="width: 120px;" />
                        <input required type="text" name="color[]" id="color" placeholder="Color" style="width: 120px;"/>
                        <input required type="number" name="price[]" id="price" placeholder="Price" style="width: 120px;" min="0" />
                        <input required type="number" name="stock[]" id="stock" placeholder="Stock" style="width: 120px;" min="0" />
                        <input required type="file" name="image[]" id="image"/>
                        <a href="javascript:void(0);" class="add_button" title="Add field">+</a>
                    </div>
                </div>  
              </div>

              
             
                <div class="form-actions">
                  <input type="submit" value="Add Attribute" class="btn btn-success">
                </div>
              </form>
              <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Attributes Table</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Attribute ID</th>
                  <th>SKU</th>
                  <th>Color</th>
                  <th>Price</th>
                  <th>Stock</th>
                  <th>Images</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($productDetails['attributes'] as $attribute)
                <tr class="gradeX">
                  <td>{{ $attribute->id }}</td>
                  <td>{{ $attribute->sku }}</td>
                  <td>{{ $attribute->color }}</td>
                  <td>{{ $attribute->price }}</td>
                  <td>{{ $attribute->stock }}</td>
                  <td>
                    <img src="{{ asset('/images/backend_images/products/small/'.$attribute->images_att)}}" style="width: 70px">
                  </td>
                  </a>
                  <td class="center"> <a href="#myModal{{ $attribute->id }}" data-toggle="modal" class="btn btn-primary btn-mini">Update</a> <a rel="{{$attribute->id}}" rel1="delete-attribute" <?php /*href="{{url('admin/delete-product/'. $product->id) }}"*/?> href="javascript:" class="btn btn-danger btn-mini deleteRecord">Delete</a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
            @foreach($productDetails['attributes'] as $attribute)
             <div id="myModal{{ $attribute->id }}" class="modal hide">
                    <div class="modal-header">
                        <button data-dismiss="modal" class="close" type="button">×</button>
                        <h3>SKU: {{ $attribute->sku }}</h3>
                    </div>
                    <div class="modal-body">
                      <div class="widget-content nopadding">
                        <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{ url('admin/update-attribute/'.$attribute->id)}}">{{csrf_field() }}
                          <div class="control-group">
                            <div class="controls">
                             <p>Attribute ID: <b>{{ $attribute->id }}</b></p>
                            </div>
                          </div>

                           <div class="control-group">
                            Color:  <div class="controls"> <input required type="text" name="color" id="color" value="{{$attribute->color}}" style="width: 120px;"/></div>
                          </div>
                          <div class="control-group">
                            Price:  <div class="controls"> <input required type="number" name="price" id="price" value="{{$attribute->price}}" style="width: 120px;" min="0" /></div>
                          </div>
                          <div class="control-group">
                            Stock:  <div class="controls"> <input required type="number" name="stock" id="stock" value="{{$attribute->stock}}" style="width: 120px;" min="0" /></div>
                          </div>
                          <div class="controls">
                            <input type="hidden" name="current_image" value="{{$attribute->images_att}}">
                            <input type="file" name="image" id="image">
                            @if(!empty($productDetails->image))
                                <img src="{{ asset('/images/backend_images/products/small/'.$attribute->images_att)}}" style="width: 70px">
                              @endif
                          </div>
                          <div class="form-actions">
                            <input type="submit" value="Update Attribute" class="btn btn-success">
                          </div>
                        </form>
                      </div>
                    </div>
                </div>
                @endforeach
          </div>
        </div>
      </div>
    </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  


@endsection