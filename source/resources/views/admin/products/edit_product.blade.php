@extends('layouts.adminLayout.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{url('/admin/dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Products</a> <a href="#" class="current">Edit Products</a> </div>
    <h1>Products</h1>
    @if(Session::has('flash_message_error'))
                
        <div class="alert alert-error alert-block">

            <button type="button" class="close" data-dismiss="alert">×</button> 

            <strong>{!!session('flash_message_error') !!}</strong>

        </div>
    @endif 
    @if(Session::has('flash_message_success'))
    
        <div class="alert alert-success alert-block">

            <button type="button" class="close" data-dismiss="alert">×</button> 

            <strong>{!!session('flash_message_success') !!}</strong>

        </div>
    @endif 
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Form validation</h5>
          </div>
          <div class="widget-content nopadding">
            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{('/admin/edit-product/'.$productDetails->id)}}" name="edit_product" id="edit_product" novalidate="novalidate">{{csrf_field() }}
              <div class="control-group">
                <label class="control-label">Category</label>
                <div class="controls">
                  <select name="category_id" id="category_id" style="width: 220px">
                   <?php echo $categories_dropdown; ?>
                  </select>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Product Name</label>
                <div class="controls">
                  <input type="text" name="product_name" id="product_name" value="{{ $productDetails->product_name }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Product Code</label>
                <div class="controls">
                  <input type="text" name="product_code" id="product_code" value="{{ $productDetails->product_code }}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Price</label>
                <div class="controls">
                  <input type="number" name="price" id="price" value="{{ $productDetails->price }}" min="0">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Sale Price</label>
                <div class="controls">
                  <div class="input-prepend"> <span class="add-on">%</span>
                    <input type="text" name="sale_price" id="sale_price" value="{{ $productDetails->price_sale }}" style="width: 180px">
                  </div>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Image</label>
                <div class="controls">
                  <input type="hidden" name="current_image" value="{{$productDetails->image}}">
                  <input type="file" name="image" id="image">
                  @if(!empty($productDetails->image))
                      <img src="{{ asset('/images/backend_images/products/small/'.$productDetails->image)}}" style="width: 70px"> | <a href="{{ url('admin/delete-product-image/'.$productDetails->id)}}">Delete</a>
                    @endif
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Description</label>
                <div class="controls">
                  <a href="#myModal" data-toggle="modal" class="btn btn-info btn-mini">Edit</a> 
                </div>
              </div>
               <div id="myModal" class="modal hide">
                  <div class="modal-header">
                    <button data-dismiss="modal" class="close" type="button">×</button>
                    <h3>Description</h3>
                  </div>
                  <div class="modal-body">
                      <textarea class="textarea_editor span12" type="text" name="description" id="description">{{ $productDetails->description }}</textarea>
                      <div class="controls">
                        <button data-dismiss="modal" class="btn btn-info btn-large" type="button">Xác nhận</button>
                      </div>
                  </div>

                </div>
              <div class="form-actions">
                <input type="submit" value="Edit Products" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


@endsection