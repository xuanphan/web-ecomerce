@if ($paginator->lastPage() > 1)
<strong>Pages:</strong>
<ol>
    @for ($i = 1; $i <= $paginator->lastPage(); $i++)
        <li @if($paginator->currentPage() == $i) class="current" @endif >
            @if($paginator->currentPage() != $i)
            <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
            @else
                {{$i}}
            @endif
        </li>
    @endfor
</ol>
@endif