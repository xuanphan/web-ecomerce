
<?php $url = url()->current(); ?>
<!-- {{Request::segment(2)}} -->
<!--sidebar-menu-->
<div id="sidebar"><a href="{{url('/admin/dashboard')}}" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
  <ul>
    <li class="submenu <?php if(preg_match("/dashboard/i", $url)){ echo " active";}?>"><a href="{{url('admin/dashboard')}}"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
    <li class="submenu"> <a href="#"><i class="icon icon-th-large"></i> <span>Categories</span> </a>
      <ul style="<?php if(preg_match("/categor/i", $url)){ echo "display: block;";}?>" >
        <li class="<?php if(preg_match("/add-category/i", $url)){ echo "active";}?>"><a href="{{url('admin/add-category')}}">Add Categories</a></li>
        <li class="<?php if(preg_match("/view-categories/i", $url)){ echo "active";}?>"><a href="{{url('admin/view-categories')}}">View Categories</a></li>
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Products</span> </a>
      <ul style="<?php if(preg_match("/product/i", $url)){ echo "display: block;";}?>">
        <li class="<?php if(preg_match("/add-product/i", $url)){ echo "active";}?>"><a href="{{url('admin/add-product')}}">Add Product</a></li>
        <li class="<?php if(preg_match("/view-products/i", $url)){ echo "active";}?>"><a href="{{url('admin/view-products')}}">View Products</a></li>
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="icon icon-list-alt"></i> <span>Coupons</span> </a>
      <ul style="<?php if(preg_match("/coupon/i", $url)){ echo "display: block;";}?>">
        <li class="<?php if(preg_match("/add-coupon/i", $url)){ echo "active";}?>"><a href="{{url('admin/add-coupon')}}">Add Coupon</a></li>
        <li class="<?php if(preg_match("/view-coupons/i", $url)){ echo "active";}?>"><a href="{{url('admin/view-coupons')}}">View Coupons</a></li>
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="icon icon-list-alt"></i> <span>Orders</span> </a>
      <ul style="<?php if(preg_match("/orders/i", $url)){ echo "display: block;";}?>">
        <li class="<?php if(preg_match("/view-orders/i", $url)){ echo "active";}?>"><a href="{{url('admin/view-orders')}}">View Orders</a></li>
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="icon icon-list-alt"></i> <span>Banners</span> </a>
      <ul style="<?php if(preg_match("/banner/i", $url)){ echo "display: block;";}?>">
        <li class="<?php if(preg_match("/add-banner/i", $url)){ echo "active";}?>"><a href="{{url('admin/add-banner')}}">Add Banner</a></li>
        <li class="<?php if(preg_match("/view-banners/i", $url)){ echo "active";}?>"><a href="{{url('admin/view-banners')}}">View Banners</a></li>
      </ul>
    </li>
  </ul>
</div>
<!--sidebar-menu-->
