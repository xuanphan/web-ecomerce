<?php 
    use App\Http\Controllers\Controller;
    $mainCategories = Controller::mainCategories();
    $countUserCart = Controller::countUserCart();
    $UserCart = Controller::userCartDetails();
    $offer = Controller:: Offer();
    $cate = Controller::getCatemenu();
 ?>
<!-- HEADER -->
    <div id="sns_header" class="wrap">
        <!-- Header Top -->
        <div class="sns_header_top">
            <div class="container">
                <div class="sns_module">
                    <div class="header-account">
                        <div class="myaccount">
                            <div class="tongle">
                                <i class="fa fa-user"></i>
                                <span>My account</span>
                                <i class="fa fa-angle-down"></i>
                            </div>
                            <div class="customer-ct content">
                                <ul class="links">
                                    @if(empty(Auth::check()))
                                    <li class=" last">
                                        <a class="top-link-login" title="Log In" href="" data-toggle="modal" data-target=".bs-example-modal-sm">Đăng nhập</a>
                                    </li>
                                    @else
                                    <li class="first">
                                        <a class="top-link-myaccount" title="My Account" href="{{url('/account')}}">@if(!empty(Session::get('frontSession'))) {{Session::get('frontSession')}} @endif</a>
                                    </li>
                                    <li>
                                        <a class="top-link-checkout" title="Checkout" href="{{url('/orders')}}">Hoá đơn tổng</a>
                                    </li>
                                    <li>
                                        <a class="top-link-logout" title="Checkout" href="{{url('/users-logout')}}">Đăng xuất</a>
                                    </li>
                                    @endif                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header Logo -->
        <div id="sns_header_logo">
            <div class="container">
                <div class="container_in">
                    <div class="row">
                        <h1 id="logo" class=" responsv col-md-3">
                            <a href="{{asset('/')}}" title="Magento Commerce">
                             <img alt="" src="{{ asset('images/frontend_images/logo.jpg')}}"> 
                            </a>
                        </h1>
                        <div class="col-md-9 policy">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-phone-12">
                                    <div class="policy_custom">
                                        <div class="policy-icon">
                                            <em class="fa fa-truck"> </em>
                                        </div>
                                        <p class="policy-titile">GIAO HÀNG MIỄN PHÍ TRÊN TOÀN QUỐC</p>
                                        <p class="policy-ct">Theo đơn đặt hàng trên 100$</p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-phone-12">
                                    <div class="policy_custom">
                                        <div class="policy-icon">
                                            <em class="fa fa-cloud-upload"> </em>
                                        </div>
                                        <p class="policy-titile">GIẢM GIÁ 20%</p>
                                        <p class="policy-ct">Vào các ngày lễ lớn</p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-phone-12">
                                    <div class="policy_custom">
                                        <div class="policy-icon">
                                            <em class="fa fa-gift"> </em>
                                        </div>
                                        <p class="policy-titile">MUA 1 TẶNG 1</p>
                                        <p class="policy-ct">Vào ngày Black Friday</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Menu -->
        <div id="sns_menu">
            <div class="container">
                <div class="sns_mainmenu">
                    <div id="sns_mainnav">
                        <div id="sns_custommenu" class="visible-md visible-lg">
                            <ul class="mainnav">
                                <li class="level0 custom-item active">
                                    <a class="menu-title-lv0 pd-menu116" href="{{asset('')}}" target="_self">
                                        <span class="title">Home</span>
                                    </a>
                                </li>
                                <li class="level0 nav-3 no-group drop-submenu12 custom-itemdrop-staticblock">
                                    <a class=" menu-title-lv0" href="{{asset('/shop-page')}}">
                                        <span class="title">Shop</span>
                                    </a>
                                    <div class="wrap_dropdown fullwidth">
                                        <div class="row">
                                            @foreach($mainCategories as $cat)
                                            @if($cat->status=="1")
                                            <div class="col-sm-3">
                                                <h6 class="title menu1-2-5"> <a href="{{ asset('products/'.$cat->url)}}">{{ $cat->name }}</a></h6>
                                                <ul class="level1">
                                                    @foreach($cat->categories as $subcat)
                                                    @if($subcat->status=="1")
                                                    <li class="level2 nav-1-3">
                                                        <a class=" menu-title-lv2" href="{{ asset('products/'.$subcat->url)}}">
                                                            <span class="title">{{ $subcat->name }}</span>
                                                        </a>
                                                    </li>
                                                    @endif
                                                    @endforeach
                                                </ul>
                                            </div>
                                            @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </li>
                                <li class="level0 custom-itemdrop-staticblock">
                                    <a class="menu-title-lv0">
                                        <span class="title">GỢI Ý</span>
                                    </a>
                                    <div class="wrap_dropdown fullwidth">
                                            
                                            <div class="row">
                                                @foreach($offer as $item)
                                                <div class="col-sm-3">
                                                    <a class="banner5" href="{{url('/product/'.$item->id)}}">
                                                        <img alt="" src="{{ asset('images/backend_images/products/small/'.$item->image)}}">
                                                    </a>
                                                    <br>
                                                    <h3 class="headtitle">{{$item->product_name}}</h3>
                                                </div>

                                            @endforeach
                                            </div>
                                    </div>
                                </li>
                                @foreach($cate as $cat)
                                <li class="level0 nav-4 no-group drop-submenu last parent">
                                    <a class=" menu-title-lv0" href="{{ url('products/'.$cat->url)}}">
                                        <span class="title">{{$cat->name}}</span>
                                    </a>
                                    <div class="wrap_submenu">
                                        <ul class="level0">
                                            @foreach($cat->categories as $subcat)
                                            <li class="level1 nav-3-1 first">
                                                <a class=" menu-title-lv1" href="{{ url('products/'.$subcat->url)}}">
                                                    <span class="title">{{$subcat->name}}</span>
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="sns_menu_right">
                        <div class="block_topsearch">
                             <div class="top-cart">
                                <div class="mycart mini-cart">
                                    <div class="block-minicart">
                                        <div class="tongle">
                                            <i class="fa fa-shopping-cart"></i>
                                            <div class="summary">
                                                <span class="amount">
                                                    <a href="{{asset('/cart')}}">
                                                        <span>{{$countUserCart}}</span>
                                                        ( sản phẩm )
                                                    </a>
                                                </span>
                                            </div>
                                        </div>
                                        @if($countUserCart > 0)
                                        <div class="block-content content">
                                            <div class="block-inner">
                                                @foreach ($UserCart->chunk(4) as $key)
                                                <ol id="cart-sidebar" class="mini-products-list">
                                                    @foreach ($key as $item)
                                                        <li class="item odd">
                                                            <a class="product-image" title="{{$item->product_name}}" href="{{url('/product/'.$item->product_id)}}">
                                                                <img alt="" src="{{ asset('images/backend_images/products/small/'.$item->product_img)}}">
                                                            </a>
                                                            <div class="product-details">
                                                                <a class="btn-remove" onclick="return confirm('Are you sure you would like to remove this item from the shopping cart?');" title="Remove This Item" href="{{url('/cart/delete-product/'.$item->id)}}">Remove This Item</a>
                                                                <p class="product-name">
                                                                    <a href="{{url('/product/'.$item->product_id)}}">{{$item->product_name}}</a>
                                                                    @if($item->quantity > 1)
                                                                        <strong>x {{$item->quantity}}</strong>
                                                                    @endif
                                                                </p>
                                                                <!-- <strong>1</strong>
                                                                x -->
                                                                <span class="price">{{number_format($item->price)}} VNĐ</span>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ol>
                                                @endforeach
                                                <?php $total_amount = 0; ?>
                                                @foreach($UserCart as $cart)
                                                    <?php $total_amount = $total_amount + ($cart->price*$cart->quantity) ?>
                                                @endforeach
                                                <p class="cart-subtotal">
                                                    <span class="label">Total:</span>
                                                    @if(!empty(Session::get('CouponAmount')))
                                                    <span class="price">{{number_format($total_amount - Session::get('CouponAmount'))}} VNĐ</span>
                                                    @else
                                                    <span class="price">{{ number_format($total_amount)}} VNĐ</span>
                                                    @endif
                                                </p>
                                                <div class="actions">
                                                    <a class="button" href="{{url('/checkout')}}">
                                                        <span>
                                                            <span>Thanh toán</span>
                                                        </span>
                                                    </a>
                                                    <a class="button gfont go-to-cart" href="{{asset('/cart')}}">Đến giỏ hàng</a>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <span class="icon-search"></span>
                            <div class="top-search">
                                <div id="sns_serachbox_pro11739847651442478087" class="sns-serachbox-pro">
                                    <div class="sns-searbox-content">
                                        <form id="search_mini_form3703138361442478087" method="get" action="{{url('/seach')}}">
                                            <div class="form-search">
                                                <input id="search3703138361442478087" class="input-text search-input" type="text" value="" name="q" placeholder="Tìm kiếm...." size="30">
                                                <button class="button form-button" title="Tìm kiếm" type="submit">Tìm kiếm</button>
                                                <div id="search_autocomplete3703138361442478087" class="search-autocomplete" style="display: none;"></div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- AND HEADER -->
<!-- BREADCRUMBS --><!-- 
    <div id="sns_breadcrumbs" class="wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="sns_titlepage"></div>
                    <div id="sns_pathway" class="clearfix">
                        <div class="pathway-inner">
                            <span class="icon-pointer "></span>
                            <ul class="breadcrumbs">
                                <li class="home">
                                    <a title="Go to Home Page" href="#">
                                        <i class="fa fa-home"></i>
                                        <span>Home</span>
                                    </a>
                                </li>
                                <li class="category3 last">
                                    <span>Funiture</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
<!-- AND BREADCRUMBS -->
<!-- POPUP REGISTER -->

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <form method="POST" id="form_login" name="form_login">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Đăng Nhập</h4>
      </div>
      <div class="modal-body">
        <div id="message-warning"></div>
          <div class="form-group">
            <label class="control-label">Email:</label>
            <input type="text" class="form-control" id="user_email" name="email">
          </div>
          <div class="form-group">
            <label class="control-label">Password:</label>
            <input class="form-control" type="password" id="user_password" name="password">
            <a class="pull-right" href="#"><u>Quên mật khẩu?</u></a>
          </div> 
      </div>
      <div class="modal-footer">
        <a href="{{ asset('/users-register')}}"><button type="button" class="pull-left btn btn-info btn-sm">Đăng ký</button></a>
        <button type="submit" class="btn btn-primary btn-sm">Đăng nhập</button>
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
     </form>
  </div>
</div>

<!-- END REGISTER -->
