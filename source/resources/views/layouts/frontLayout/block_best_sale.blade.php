
<?php 
   use App\Http\Controllers\Controller;
   $sale = Controller:: getbestSale();
   // $count = count($sale);
   // echo "<pre>"; echo($count); exit();
 ?>
    <div class="content">
       
        <div id="products_slider12" class="products-slider12 owl-carousel owl-theme" style="display: inline-block">
            @if(count($sale)<=3)
                @foreach ($sale as $item)
                <div class="item-row">
                    <div class="item">
                        <div class="item-inner">
                            <div class="prd">
                                <div class="item-img clearfix">
                                    <a class="product-image have-additional" href="{{url('/product/'.$item->id)}}" title="{{$item->product_name}}">
                                        <span class="img-main">
                                            <img alt="" src="{{ asset('images/backend_images/products/small/'.$item->image)}}">
                                        </span>
                                    </a>
                                </div>
                                <div class="item-info" style="width: 45%;">
                                    <div class="info-inner" style="height: 10%;">
                                        <div class="item-title" >
                                            <a href="{{url('/product/'.$item->id)}}" title="{{$item->product_name}}"> {{$item->product_name}} </a>
                                        </div>
                                        <div class="item-price">
                                            <span class="price">
                                                <span class="price1">sale off {{$item->price_sale}}%</span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="action-bot" style="width: 105%;">
                                        <div class="wrap-addtocart">
                                            <button class="btn-cart" title="Add to Cart">
                                                <i class="fa fa-shopping-cart"></i>
                                                <span>Add to Cart</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            @else
            @foreach ($sale->sortByDesc('price_sale')->chunk(3) as $key)
            <div class="item-row">
                 @foreach ($key->sortByDesc('price_sale') as $item)
                <div class="item">
                    <div class="item-inner">
                        <div class="prd">
                            <div class="item-img clearfix">
                                <a class="product-image have-additional" href="{{url('/product/'.$item->id)}}" title="{{$item->product_name}}">
                                    <span class="img-main">
                                        <img alt="" src="{{ asset('images/backend_images/products/small/'.$item->image)}}">
                                    </span>
                                </a>
                            </div>
                            <div class="item-info" style="width: 45%;">
                                <div class="info-inner" style="height: 10%;">
                                    <div class="item-title" >
                                        <a href="{{url('/product/'.$item->id)}}" title="{{$item->product_name}}"> {{$item->product_name}} </a>
                                    </div>
                                    <div class="item-price">
                                        <span class="price">
                                            <span class="price1">sale off {{$item->price_sale}}%</span>
                                        </span>
                                    </div>
                                </div>
                                <div class="action-bot" style="width: 105%;">
                                    <div class="wrap-addtocart">
                                        <button class="btn-cart" title="Add to Cart">
                                            <i class="fa fa-shopping-cart"></i>
                                            <span>Add to Cart</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
          @endforeach 
          @endif 
        </div>
        
    </div>
