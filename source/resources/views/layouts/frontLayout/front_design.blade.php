<!DOCTYPE html>
<html lang="en-US">
    <head>
        <title>Home Page2</title>
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet">
        <!-- Style Sheet-->
        <link rel="stylesheet" type="text/css" href="{{ asset('font/frontend_font/css/font-awesome.min.css')}}" />
        <link rel="stylesheet" href="{{ asset('css/frontend_css/bootstrap.css')}}">
        <link rel="stylesheet" href="{{ asset('css/frontend_css/style.css')}}">
        <link rel="stylesheet" href="{{ asset('js/frontend_js/owl.carousel.css')}}">
        <link rel="stylesheet" href="{{ asset('js/frontend_js/owl.theme.css')}}">
        <link rel="stylesheet" href="{{ asset('css/frontend_css/easyzoom.css')}}">
        <link rel="stylesheet" href="{{ asset('css/frontend_css/passtrength.css')}}">
        <link rel="stylesheet" href="{{ asset('css/frontend_css/datepicker.css')}}">
        <!-- favicon -->
        <link rel="shortcut icon" href="{{ asset('images/frontend_images/favicon.ico')}}">
        <!-- META TAGS -->
        <meta name="viewport" content="width=device-width" />
    </head>
    <body id="bd" class=" cms-index-index2 header-style2 cms-simen-home-page-v2 default cmspage">
        <div id="sns_wrapper">      
            @include('layouts.frontLayout.front_header')

            @yield('content')

            @include('layouts.frontLayout.front_footer')        
        </div>
            <!-- Scripts -->
            <!-- <script src="//code.jquery.com/jquery-1.10.2.js"></script> -->
            <!-- <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->
            <script src="{{ asset('js/frontend_js/jquery-1.9.1.min.js')}}"></script>
            <script src="{{ asset('js/frontend_js/jquery-ui.js')}}"></script>
            <script src="{{ asset('bootstrap/js/bootstrap.js')}}"></script>
            <script src="{{ asset('js/frontend_js/owl.carousel.min.js')}}"></script>
            <script src="{{ asset('js/frontend_js/sns-extend.js')}}"></script>
            <script src="{{ asset('js/frontend_js/custom.js')}}"></script>
            <script src="{{ asset('js/backend_js/jquery.validate.js') }}"></script>
            <script src="{{ asset('js/frontend_js/list-grid.js')}}"></script>
            <script src="{{ asset('js/frontend_js/passtrength.js')}}"></script>
            <script src="{{ asset('js/frontend_js/bootstrap-datepicker.js')}}"></script>
            <script src="{{ asset('js/backend_js/jquery.wizard.js')}}"></script> 
            <script src="{{ asset('js/backend_js/matrix.wizard.js')}}"></script>
            <script src="{{ asset('js/frontend_js/typeahead.js')}}"></script>
            <script type="text/javascript">
                $('#bitrh_day').datepicker();
            </script>
    </body>
</html>