<!-- sns_left -->
<div id="sns_left" class="col-md-3">
    <div class="wrap-in">
        <div class="block block-layered-nav block-layered-nav--no-filters">
            <div class="block-content toggle-content">
                <dl id="narrow-by-list">
                    <dt class="odd">Categories</dt>
                          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            
                             <?php //echo $categories_menu ?>
                             @foreach($categories as $cat)
                             @if($cat->status=="1")
                             <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                      <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#{{ $cat->id }}" aria-expanded="false" aria-controls="collapseTwo">
                                            {{ $cat->name}}
                                        </a>
                                      </h4>
                                    </div>
                                    <div id="{{ $cat->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        @foreach($cat->categories as $subcat)
                                            @if($subcat->status=="1")
                                            <div class="panel-body">
                                                <dd class='odd'>
                                                    <ol>
                                                        <li>
                                                            <a href="/products/{{$subcat->url}}">
                                                                {{$subcat->name}}
                                                            </a>
                                                        </li>
                                                    </ol>
                                                </dd>
                                            </div>
                                            @endif
                                       @endforeach
                                    </div>
                                  </div>
                            @endif
                            @endforeach
                                </div>
                </dl>
            </div>
        </div>
        <div class="block block_cat">
            <a class="banner5" href="#">
                <img src="{{ asset('images/frontend_images/banner_right.jpg')}}" alt="">
            </a>
        </div>



    </div>
</div>
<!-- sns_left -->