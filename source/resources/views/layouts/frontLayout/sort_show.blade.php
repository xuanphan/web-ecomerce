<div class="limiter">
 <label>Show</label>
 <div class="select-new">
     <div class="select-inner jqtransformdone">
         <div class="jqTransformSelectWrapper" style="z-index: 10; width: 80px;">
             <div class="Sort">
                 <span style="width: 50px;" id="sort-by"> </span>
                 <a class="jqTransformSelectOpen" href="#"></a>
             </div>
             <ul style="width: 78px; display: none; visibility: visible;">
                <li>
                    <a id="8" class="selected"> 8 </a>
                </li>
                <li>
                    <a id="32"> 32 </a>
                </li>
                <li>
                    <a id="64"> 64 </a>
                </li>
                <li>
                    <a id="100"> 100 </a>
                </li>
             </ul>
         </div>
     </div>
 </div>
 <span>per page</span>
</div>
<div class="sort-by">
 <label>Sort by</label>
 <div class="select-new">
     <div class="select-inner jqtransformdone">
         <div class="jqTransformSelectWrapper" style="z-index: 10; width: 118px;">
             <div>
                 <span style="width: 50px;"> Position </span>
                 <a class="jqTransformSelectOpen" href="#"></a>
             </div>
             <ul style="width: 116px; display: none; visibility: visible;">
                 <li class="active">
                     <a class="selected" href="#"> Position </a>
                 </li>
                 <li>
                     <a href="#"> Name </a>
                 </li>
                 <li>
                     <a href="#"> Price </a>
                 </li>
             </ul>
             <select class="select-sort-by jqTransformHidden" onchange="setLocation(this.value)" style="">
                 <option selected="selected"> Position </option>
                 <option> Name </option>
                 <option> Price </option>
             </select>
         </div>
     </div>
 </div>