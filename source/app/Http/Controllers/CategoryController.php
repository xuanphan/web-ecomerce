<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;

class CategoryController extends Controller
{
    public function addCategory(Request $request){

    	if($request->isMethod('post')){
    		$data = $request->all();
    		//echo "<pre>"; print_r($data); die;
            if(empty($data['status'])){
                $status = 0; 
            }else{
                $status = 1;
            }
    		$category = new Category;
    		$category->name = $data['category_name'];
            $category->parent_id = $data['parent_id'];
    		$category->description = $data['description'];
    		$category->url = $data['url'];
            $category->status = $status;
    		$category->save();
            return redirect('/admin/view-categories')->with('flash_message_success','Thêm thành công !');
    	}
        $levels = Category::where(['parent_id'=>0])->get();
    	return view('admin.categories.add_category')->with(compact('levels'));
    }
    public function editCategory(Request $request, $id = null){
        if($request->isMethod('post')){
            $data = $request->all();
            if ($data['parent_id']==$id) {
                return redirect()->back()->with('flash_message_error','Danh mục không thể chứa chính nó! Hãy chọn danh mục cha khác');
            }else{
                if(empty($data['status'])){
                $status = 0; 
                }else{
                    $status = 1;
                }
                Category::where(['id'=>$id])->update(['name'=>$data['category_name'],'description'=>$data['description'],'url'=>$data['url'],'status'=>$status,'parent_id'=>$data['parent_id']]);
                return redirect('/admin/view-categories')->with('flash_message_success','Đã sửa thành công');
            }
            
        }
        $categoryDetails = Category::where(['id'=>$id])->first();
        $levels = Category::where(['parent_id'=>0])->get();
        return view('admin.categories.edit_category')->with(compact('categoryDetails','levels'));
    }
    public function deleteCategory($id = null){
        //$product = Product::where(['category_id'=>$id])->first();
        //var_dump($product); die;
        if(!empty($id)){
            $level = Category::where(['parent_id'=>$id])->first();
            //echo "<pre>"; var_dump($level); exit();
            if ($level == NULL) {
                //echo "yes"; exit();
                Product::where(['category_id'=>$id])->update(['category_id'=>NULL]);
                Category::where(['id'=>$id])->delete();
            }else{
                //echo "no"; exit();
                Category::where(['parent_id'=>$id])->update(['parent_id'=>0,'status'=>0]);
                Product::where(['category_id'=>$id])->update(['category_id'=>NULL]);
                Category::where(['id'=>$id])->delete();
            }
           
            return redirect()->back()->with('flash_message_success','Đã xoá thành công');
        }
    }
    public function viewCategories(){
        $categories = Category::get();
        return view('admin.categories.view_categories')->with(compact('categories'));
    }
}
