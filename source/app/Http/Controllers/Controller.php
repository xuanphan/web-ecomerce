<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Auth;
use App\Category;
use App\Product;
use Session;
use App\Banner;
use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function mainCategories(){
    	$mainCategories = Category::where(['parent_id' =>0])->get();
    	// echo "<pre>"; print_r(($mainCategories)); die;
    	
    	return $mainCategories;
    }
    public static function countUserCart(){
        if (Auth::check()) {
            $user_email = Auth::user()->email;
            if (Session::get('session_id') != null) {
                $countUserCart = DB::table('cart')->where(['user_email'=>$user_email])->orwhere('session_id',Session::get('session_id'))->count();
            }else{
                $countUserCart = DB::table('cart')->where(['user_email'=>$user_email])->count();
            }
            
        }else
    	{
            $session_id = Session::get('session_id');
            $countUserCart = DB::table('cart')->where(['session_id'=>$session_id])->count();
        }
    	return $countUserCart;
    }
    public static function userCartDetails(){
        if (Auth::check()) {
            $user_email = Auth::user()->email;
            if (Session::get('session_id') != null) {
                $userCartDetails = DB::table('cart')->where(['user_email'=>$user_email])->orwhere('session_id',Session::get('session_id'))->get();
            }else{
                $userCartDetails = DB::table('cart')->where(['user_email'=>$user_email])->get();
            }
            
        }else
        {
            $session_id = Session::get('session_id');
            $userCartDetails = DB::table('cart')->where(['session_id'=>$session_id])->get();
        }
    	return $userCartDetails;
    }
    public static function Offer(){
        $productsOffer = Product::inRandomOrder()->take(4)->get();
        return $productsOffer;
    }
    public static function Partner(){
        $partner = Banner::where(['type'=>'3','status'=>'1'])->get();
        return $partner;
    }
    public static function countPartner(){
        $num = Banner::where(['type'=>'3','status'=>'1'])->count();
        return $num;
    }
    public static function getbestSale(){
        $sale = Product::where('price_sale','!=','')->take(12)->get();
        return $sale;
    }
    public static function getCatemenu(){
        $cate = Category::where(['parent_id' =>0])->inRandomOrder()->take(4)->get();
        return $cate;
    }
}
