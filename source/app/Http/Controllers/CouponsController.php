<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Coupon;

class CouponsController extends Controller
{
    public function addCoupon(Request $request){
    	if ($request->isMethod('post')) {
    		$data = $request->all();
    		// echo "<pre>"; var_dump($data); exit();
    		$coupon = new Coupon;
    		$coupon->coupon_code = $data['coupon_code'];
    		$coupon->amount = $data['amount'];
    		$coupon->amount_type = $data['amount_type'];
    		$date = date("Y-m-d", strtotime($data['expiry_date']));
    		$coupon->expiry_date = $date;
    		if (empty($data['status'])) {
    			$data['status'] = 0 ;
    		}
    		$coupon->status = $data['status'];
    		$coupon->save();
    		return redirect()->action('CouponsController@viewCoupons')->with('flash_message_success','Đã thêm thành công phiếu mua hàng!');
    	}
    	return view('admin.coupons.add_coupon');
    }
    public function updateCoupon(Request $request, $id=null){
    	$couponDetails = Coupon::find($id);
 	   	//echo "<pre>"; var_dump($couponDetails);
    	if ($request->isMethod('post')) {
    		$data = $request->all();
    		$coupon = Coupon::find($id);
    		$coupon->coupon_code = $data['coupon_code'];
    		$coupon->amount = $data['amount'];
    		$coupon->amount_type = $data['amount_type'];
            $date = date("Y-m-d",strtotime($data['expiry_date']));
    		$coupon->expiry_date = $date;
    		if (empty($data['status'])) {
    			$data['status'] = 0 ;
    		}
    		$coupon->status = $data['status'];
    		$coupon->save();
    		return redirect()->action('CouponsController@viewCoupons')->with('flash_message_success','Đã cập nhật thành công phiếu mua hàng!');
    	}
    	return view('admin.coupons.update_coupon')->with(compact('couponDetails'));
    }
    public function deleteCoupon($id=null){
    	Coupon::where(['id'=>$id])->delete();
    	return redirect()->back()->with('flash_message_success','Đã xoá thành công phiếu mua hàng!');
    }
    public function viewCoupons(){
    	$coupons = Coupon::get();
    	return view('admin.coupons.view_coupons')->with(compact('coupons'));
    }
}
