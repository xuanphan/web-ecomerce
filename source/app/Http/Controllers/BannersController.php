<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Banner;
use Image;

class BannersController extends Controller
{
    public function addBanner(Request $request){
    	if ($request->isMethod('post')) {
    		$data = $request->all();
    		///echo "<pre>"; var_dump($data); exit();
    		$banner = new Banner;
    		
    		$banner->title = $data['banner_title'];
    		$banner->link = $data['link'];
    		if (empty($data['status'])) {
    			$status = '0';
    		}else{
    			$status = '1';
    		}
    		//Up hình
    		if($request->hasFile('image')){
    			$image_tmp = Input::file('image');
    			if ($image_tmp->isValid()) {
    				//echo "test";die;
    				$extension = $image_tmp->getClientOriginalExtension();
    				$filename = rand(111,999).'.'.$extension;
    				$banner_path = 'images/frontend_images/banners/'.$filename;    
    				//Định dạng lại cỡ hình
    				Image::make($image_tmp)->save($banner_path);
    				//Lưu tên hình vào database
    				$banner->image = $filename;
    			}
    		}
            $banner->type = $data['type_banner'];
    		$banner->status = $status;
    		$banner->save();
    		return redirect()->back()->with('flash_message_success','Đã thêm slide thành công!');
    	}
    	return view('admin.banners.add_banner');
    }
    public function viewBanners(){
    	$banners = Banner::get();
        $bannerslide = Banner::where('type','1')->count();
        $bannerStatus = Banner::where(['status'=>'1','type'=>'1'])->count();
    	return view('admin.banners.view_banners')->with(compact('banners','bannerStatus','bannerslide'));
    }
    public function updateBanner(Request $request){
        if ($request->isMethod('post')) {
            $data = $request->all();
            $bannerDetail = Banner::where('id',$data['banner-id'])->first();
            if (empty($data['status'])) {
                if($bannerDetail->type == '1'){
                    $countSlide = Banner::where(['type'=>'1','status'=>'1'])->count();
                    if ($countSlide <= 2) {
                        return redirect('admin/view-banners')->with('flash_message_error','Cập nhật slide không thành công! Phải hiển thị tối thiểu 2 slide!');
                        exit();
                    }else{
                        $status = '0';
                    }
                }else{
                    $status = '0';
                }
            }else{
                $status = '1';
            }
            if (empty($data['title'])) {
               $data['title'] = '';
            }
            if (empty($data['link'])) {
                $data['link'] = '';
            }
            $current_image = $data['current_image'];
            // echo "<pre>"; var_dump($data); exit();
            if ($request->hasFile('image')) {
                $banner_path = 'images/frontend_images/banners/';
                //echo $banner_path.$data['current_image']; exit();
                if(file_exists($banner_path.$current_image)){
                    unlink($banner_path.$current_image);
                }
                $image_tmp = Input::file('image');
                if ($image_tmp->isValid()) {
                    $filename = $image_tmp->getClientOriginalName();
                    Image::make($image_tmp)->save($banner_path.$filename);
                }
            }else if(!empty($data['current_image'])) {
                    $filename = $data['current_image'];
                }else{
                    $filename = '';
                }
            }
        Banner::where('id',$data['banner-id'])->update(['status'=>$status,'title'=>$data['title'],'link'=>$data['link'],'image'=>$filename]);
        return redirect('admin/view-banners')->with('flash_message_success','Đã cập nhật banner thành công!');
    }
    public function deleteBanner($id=null){
        $banners = Banner::get();
        $countSlide = Banner::where(['status'=>'1','type'=>'1'])->count();
        //echo $countBanners; exit();
        $bannerDetail = Banner::where('id',$id)->first();
        if ($bannerDetail->type == '1') {
            if($bannerDetail->status == '1'){
                if ($countSlide > 2) {
                    if ($bannerDetail->image!=NULL) {
                        $banner_path = 'images/frontend_images/banners/';
                            if(file_exists($banner_path.$bannerDetail->image)){
                                unlink($banner_path.$bannerDetail->image);
                            }
                        Banner::where('id',$id)->delete();
                    }else{
                        Banner::where('id',$id)->delete();
                    }
                    return redirect()->back()->with('flash_message_success','Đã xoá slide thành công');
                }else{
                    return redirect()->back()->with('flash_message_error','Xoá slide không thành công! Phải để lại 2 slide!');
                }
            }
            else{
                if ($bannerDetail->image!=NULL) {
                    $banner_path = 'images/frontend_images/banners/';
                        if(file_exists($banner_path.$bannerDetail->image)){
                            unlink($banner_path.$bannerDetail->image);
                        }
                    Banner::where('id',$id)->delete();
                }else{
                    Banner::where('id',$id)->delete();
                }
                return redirect()->back()->with('flash_message_success','Đã xoá slide thành công');
            }
        }  
    } 
    public function bannerindex(Request $request){
        return view('admin.banners.banner_index');
    }      
}
