<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Exports\OrdersExport;
use App\Exports\OrderDetailsExport;
use Maatwebsite\Excel\Facades\Excel;
use Dompdf\Adapter\CPDF;      
use Dompdf\Dompdf;
use Dompdf\Exception;

use Auth;
use Session;
use Image;
use App\Category;
use App\Product;
use App\ProductsAttribute;
use App\Http\Requests;
use App\Coupon;
use App\User;
use App\Deliveries;
use App\Orders;
use App\Order_products;
use App\Cancels;
use DB;
class ProductsController extends Controller
{
    public function addProduct(Request $request)
    {
    	if ($request->isMethod('post')) {
    		$data = $request->all();
    		$product = new Product;
    		if (empty($data['category_id'])) {
    			return redirect()->back()->with('flash_message_error','Vui lòng chọn danh mục !');
    		}
    		$product->category_id = $data['category_id'];
    		$product->product_name = $data['product_name'];
    		$product->product_code = $data['product_code'];
    		
    		if (!empty($data['description'])) {
    			$product->description = $data['description'];
    		}else{
    			$product->description = '';
    		}
    		$product->price = $data['price'];
            if (empty($data['sale_price'])) {
                $data['sale_price'] = '';
            }
            $product->price_sale = $data['sale_price'];
    		//Up hình
    		if($request->hasFile('image')){
    			$image_tmp = Input::file('image');
    			if ($image_tmp->isValid()) {
    				//echo "test";die;
    				$extension = $image_tmp->getClientOriginalExtension();
    				$filename = rand(111,999).'.'.$extension;
    				$large_image_path = 'images/backend_images/products/large/'.$filename;
    				$small_image_path = 'images/backend_images/products/small/'.$filename;
    				//Định dạng lại cỡ hình
    				Image::make($image_tmp)->save($large_image_path);
    				Image::make($image_tmp)->resize(250,250)->save($small_image_path);
    				//Lưu tên hình vào database
    				$product->image = $filename;
    			}
    		}

    		$product->save();
    		//return redirect()->back()->with('flash_message_success','Thêm thành công !');
            return redirect('/admin/view-products')->with('flash_message_success','Thêm thành công !');
    	}


    	$categories = Category::where(['parent_id'=>0])->get();
    	$categories_dropdown = "<option selected disabled>--danh mục--</option>";
    	foreach ($categories as $cat) {
    		$categories_dropdown .= "<option value=' ".$cat->id."'>".$cat->name."</option>";
    		$sub_categories = Category::where(['parent_id'=>$cat->id])->get();
    		foreach ($sub_categories as $sub_cat) {
    			$categories_dropdown .= "<option value=' ".$sub_cat->id."'>&nbsp;--&nbsp;".$sub_cat->name."</option>";    		}
    	}


    	return view('admin.products.add_product')->with(compact('categories_dropdown'));
    }
    public function editProduct(Request $request, $id=null){

        if($request->isMethod('post')){
            $data = $request->all();

            //Up hình
            if($request->hasFile('image')){
                $image_tmp = Input::file('image');
                if ($image_tmp->isValid()) {
                    //echo "test";die;
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = rand(111,999).'.'.$extension;
                    $large_image_path = 'images/backend_images/products/large/'.$filename;
                    $small_image_path = 'images/backend_images/products/small/'.$filename;
                    //Định dạng lại cỡ hình
                    Image::make($image_tmp)->save($large_image_path);
                    Image::make($image_tmp)->resize(250,250)->save($small_image_path);
                }
            }else{
                if($data['current_image'] != NULL){
                    $filename = $data['current_image'];
                }
                else{
                    $filename = '';
                }
                
            }
            if (empty($data['description'])) {
                # code...
                $data['description']='';
            }
            if (empty($data['sale_price'])) {
                $data['sale_price'] = '';
            }
            Product::where(['id'=>$id])->update(['category_id'=>$data['category_id'],'product_name'=>$data['product_name'],'product_code'=>$data['product_code'],'description'=>$data['description'],'price'=>$data['price'],'image'=>$filename,'price_sale'=>$data['sale_price']]);
            return redirect()->back()->with('flash_message_success','Đã sửa thành công!');
        }

        $productDetails = Product::where(['id'=>$id])->first();
        $categories = Category::where(['parent_id'=>0])->get();
        $categories_dropdown = "<option selected disabled>--danh mục--</option>";
        foreach ($categories as $cat) {
            if($cat->id == $productDetails->category_id){
                $selected = "selected";
            }else{
                $selected = "";
            }
            $categories_dropdown .= "<option value=' ".$cat->id."'".$selected.">".$cat->name."</option>";
            $sub_categories = Category::where(['parent_id'=>$cat->id])->get();
            foreach ($sub_categories as $sub_cat) {
                if($sub_cat->id == $productDetails->category_id){
                    $selected = "selected";
                }else{
                    $selected = "";
                }
                $categories_dropdown .= "<option value=' ".$sub_cat->id."'".$selected.">&nbsp;--&nbsp;".$sub_cat->name."</option>";          }
        }
       
        return view('admin.products.edit_product')->with(compact('productDetails','categories_dropdown'));

    }
    public function deleteProductImage($id = null){
        //lấy tên hình
        $productImage = Product::where(['id'=>$id])->first();
        //lấy loại hình
        $large_image_path = 'images/backend_images/products/large/';
        $small_image_path = 'images/backend_images/products/small/';
        //
        if(file_exists($large_image_path.$productImage->image)){
            unlink($large_image_path.$productImage->image);
        }
        //
        if(file_exists($small_image_path.$productImage->image)){
            unlink($small_image_path.$productImage->image);
        }

        Product::where(['id'=>$id])->update(['image'=>'']);
        return redirect()->back()->with('flash_message_success','Đã xoá ảnh thành công!');
    }
    public function deleteProduct($id = null){
        $attrcount = ProductsAttribute::where(['product_id'=>$id])->count();
        if ($attrcount>0) {
            return redirect()->back()->with('flash_message_error','Sản phẩm còn tồn tại attribute!');
        }else{
            $this->deleteProductImage($id);
            Product::where(['id'=>$id])->delete();
            return redirect()->back()->with('flash_message_success','Đã xoá thành công!');
        }
       
    }
    public function viewProducts(){
        $products = Product::get();
        foreach ($products as $key => $value) {
            if($value->category_id !=NULL){
            $category_name = Category::where(['id'=>$value->category_id])->first();
            $products[$key]->category_name = $category_name->name;
        }else{
                $products[$key]->category_name = '';
            }
        }

        return view('admin.products.view_products')->with(compact('products'));
    }
    //thêm thuộc tính sản phẩm
    public function addAttributes(Request $request,$id=null){
        $productDetails = Product::with('attributes')->where(['id'=>$id])->first();
            //echo "<pre>"; print_r($productDetails); die;
        if($request->isMethod('post')){
            $data = $request->all();
            //var_dump($data); die;
            //echo "<pre>"; print_r($data); die;
            foreach ($data['sku'] as $key => $val) {
                //var_dump($data['image'][$key]); die;
                if(!empty($val)){
                    //Kiểm tra mã SKU
                    $attrCountSKU = ProductsAttribute::where('sku',$val)->count();
                    if($attrCountSKU>0){
                        return redirect('admin/add-attribute/'.$id)->with('flash_message_error','Mã SKU đã tồn tại! Hãy nhập mã SKU khác.');
                    }
                    //Kiểm tra màu sắc
                    $attrCountColor = ProductsAttribute::where(['product_id'=>$id,'color'=>$data['color'][$key]])->count();
                    if($attrCountColor>0){
                        return redirect('admin/add-attribute/'.$id)->with('flash_message_error','Màu '.$data['color'][$key].' đã có sẵn! Hãy nhập màu khác.');
                    }
                    $attribute = new ProductsAttribute;
                    //Up hình attribute
                    if($request->hasFile('image')){
                        //$image_tmp = Input::file('image');
                        //$image_tmp =  $request->image;
                        
                        if ($data['image'][$key]->isValid()) {
                           //var_dump($data['image'][$key]); die;
                            $extension = $data['image'][$key]->getClientOriginalExtension();
                            //var_dump($extension); die;
                            $filename = rand(111,999).'.'.$extension;
                            $large_image_path = 'images/backend_images/products/large/'.$filename;
                            $small_image_path = 'images/backend_images/products/small/'.$filename;
                            //Định dạng lại cỡ hình
                            Image::make($data['image'][$key])->save($large_image_path);
                            Image::make($data['image'][$key])->resize(250,250)->save($small_image_path);
                            //Lưu tên hình vào database

                            $attribute->images_att = $filename;
                            
                        }
                    }
                    
                    $attribute->product_id = $id;
                    $attribute->sku = $val;
                    $attribute->color = $data['color'][$key];
                    $attribute->price = $data['price'][$key]; 
                    $attribute->stock = $data['stock'][$key];
                    $attribute->save();
                }
            }
            return redirect('admin/add-attribute/'.$id)->with('flash_message_success','Thêm thành công!');
        }
        return view('admin.products.add_attributes')->with(compact('productDetails'));
    }
    public function updateAttributes(Request $request, $id = null){
        $attDetail = ProductsAttribute::where(['id'=>$id])->first();
        if ($request->isMethod('post')) {
           $data = $request->all();
           //echo "<pre>"; var_dump($data); exit();
            //Up hình
            if($request->hasFile('image')){
                $filename = $attDetail->images_att;
                $large_image_path = 'images/backend_images/products/large/';
                $small_image_path = 'images/backend_images/products/small/';
                //Xoá hình cũ
                if(file_exists($large_image_path.$filename)){
                    unlink($large_image_path.$filename);
                }
                //
                if(file_exists($small_image_path.$filename)){
                    unlink($small_image_path.$filename);
                }
                $image_tmp = Input::file('image');
                if ($image_tmp->isValid()) {

                    //Định dạng lại cỡ hình
                    Image::make($image_tmp)->save($large_image_path.$filename);
                    Image::make($image_tmp)->resize(250,250)->save($small_image_path.$filename);
                }
            }
            ProductsAttribute::where(['id'=>$id])->update(['color'=>$data['color'],'price'=>$data['price'],'stock'=>$data['stock']]);
            return redirect()->back()->with('flash_message_success','Đã sửa thành công!');
        }
    }
    public function deleteAttributes($id = null){
        $AtrributeImage = ProductsAttribute::where(['id'=>$id])->first();
        //lấy loại hình
        $large_image_path = 'images/backend_images/products/large/';
        $small_image_path = 'images/backend_images/products/small/';
        //
        if(file_exists($large_image_path.$AtrributeImage->images_att)){
            unlink($large_image_path.$AtrributeImage->images_att);
        }
        //
        if(file_exists($small_image_path.$AtrributeImage->images_att)){
            unlink($small_image_path.$AtrributeImage->images_att);
        }
        ProductsAttribute::where(['id'=>$id])->delete();
        return redirect()->back()->with('flash_message_success','Đã xoá thành công!');
    }
    public function products($url = null){
        //Trang 404
        $countCategrory = Category::where(['url'=> $url,'status'=>1])->count();
        //echo $countCategrory; die;
        if($countCategrory==0){
            abort(404);
        }

        $categories = Category::with('categories')->where(['parent_id'=>0])->get();

        $categoriesDetails = Category::where(['url'=> $url])->first();

        if($categoriesDetails->parent_id==0){
            //nếu url là url danh mục chính
                $subCategories = Category::where(['parent_id'=>$categoriesDetails->id])->get();
                $countsubCategories = Category::where(['parent_id'=>$categoriesDetails->id])->count();
                if ($countsubCategories == 0) {
                    abort(404);
                }
                foreach ($subCategories as $subcat) {
                    $cat_ids[] = $subcat->id;
                }
                // echo $cat_ids;
                $productsAll = Product::whereIn('category_id',$cat_ids)->paginate(8);
                $countPro = Product::whereIn('category_id',$cat_ids)->count();

        }else{
            //nếu url là url danh mục con
            $productsAll = Product::where(['category_id' => $categoriesDetails->id])->paginate(8);
            $countPro = Product::where(['category_id' => $categoriesDetails->id])->count();
        }

        
        return view('products.listing')->with(compact('categoriesDetails','productsAll','categories','countPro'));
    }
    public function product($id = null){
        $productDetails = Product::with('attributes')->where('id',$id)->first();
         //var_dump($productDetails); die;
        $productCount = Product::where(['id'=>$id])->count();
        if($productCount == 0){
            abort(404);
        }
        //sản phẩm cùng loại
        $relateProducts = Product::where('id','!=',$id)->where(['category_id'=>$productDetails->category_id])->get();
        //echo "<pre>"; var_dump($relateProducts); exit();
        // foreach ($relateProducts->chunk(5) as $key) {
        //     foreach ($key as $item) {
        //         echo $item; echo "<br>";
        //     }
        //     echo "<br><br><br>";
        // }
        // exit();

        //Số lượng tồn kho
        $total_stock = ProductsAttribute::where('product_id',$id)->sum('stock');
        //echo $total_stock; exit();
        return view('products.detail')->with(compact('productDetails','total_stock','relateProducts'));
    }
    public function getProductPrice(Request $request){ 
        $data = $request->all();
        //echo("<pre>"); var_dump($data); die;
        $productAtri = explode("-", $data['id']);
        //echo $productAtri[0]; echo $productAtri[1]; die;
        $productAtri = ProductsAttribute::where(['product_id'=>$productAtri[0],'color'=>$productAtri[1]])->first();
        $product = Product::where('id', $productAtri->product_id)->first();
        //var_dump($product->price_sale); exit();
        if(!empty( $product->price_sale)){
            $price = $productAtri->price - ($productAtri->price * $product->price_sale)/100;
        }else{
            $price = $productAtri->price;
        }
        echo $price;
        echo "#";
        echo $productAtri->stock;  
    }
    public function addtocart(Request $request){
        $data = $request->all();
        //echo "<pre>"; var_dump($data); die;
        
        if (empty(Auth::check()->email)) {            
            $data['user_email'] = '';
        }else{
            $data['user_email']= Auth::user()->email;
        }
        $session_id = Session::get('session_id');
        if (empty($session_id)) {
            $session_id = str_random(40);
            Session::put('session_id',$session_id);
        }
        if ($data['color']==NULL) {
            return redirect()->back()->with('flash_message_error','Xin hãy chọn màu sắc');
        }
        $colorArr = explode("-", $data['color']);
        $productImg = ProductsAttribute::where(['product_id'=>$colorArr[0],'color'=>$colorArr[1]])->first();
        $ArrImg = $productImg->images_att;
        if ($data['quantity']>$productImg->stock) {
            return redirect()->back()->with('flash_message_error','Hiện tại chỉ còn '.$productImg->stock.' sản phẩm '.$data['product_name'].' '.$colorArr[1].'! Hãy chọn màu khác!');
        }
        //var_dump($ArrImg); exit();
        $countProducts = DB::table('cart')->where(['product_id'=>$data['product_id'],'product_color'=>$colorArr[1],'session_id'=>$session_id])->count();
        if ($countProducts>0) {
            $cart = DB::table('cart')->where(['product_id'=>$data['product_id'],'product_color'=>$colorArr[1]])->first();
            $qty = $cart->quantity + $data['quantity'];
            if ($qty > $productImg->stock) {
                return redirect()->back()->with('flash_message_error','Hiện sản phẩm '.$data['product_name'].' '.$colorArr[1].' bạn đã đặt hết! Hãy chọn màu khác!');
            }else{
                DB::table('cart')->update(['quantity'=>$qty]);
            }
            
        }else{

            $getSKU  = ProductsAttribute::select('sku')->where(['product_id'=>$data['product_id'],'color'=>$colorArr[1]])->first();
            DB::table('cart')->insert(['product_id'=>$data['product_id'],'product_name'=>$data['product_name'],'product_code'=>$getSKU->sku,'price'=>$data['price'],'product_color'=>$colorArr[1],'quantity'=>$data['quantity'],'user_email'=>$data['user_email'],'session_id'=>$session_id,'product_img'=>$ArrImg]);
        }
       
        return redirect('cart')->with('flash_message_success',$data['product_name'].' đã được thêm vào giỏ!');
    }
    public function cart(){
        
        if (Auth::check()) {
            $user_email = Auth::user()->email;
            if (Session::get('session_id') != null) {
                $userCart = DB::table('cart')->where(['user_email'=>$user_email])->orwhere('session_id',Session::get('session_id'))->get();
            }else{
                $userCart = DB::table('cart')->where(['user_email'=>$user_email])->get();
            }
        }else{
            $session_id = Session::get('session_id');
            $userCart = DB::table('cart')->where(['session_id'=>$session_id])->get();
        }
        return view('products.cart')->with(compact('userCart'));
    }
    public function deleteCartProduct($id = null){
        $name = DB::table('cart')->where('id',$id)->first();
        DB::table('cart')->where('id',$id)->delete();
        return redirect()->back()->with('flash_message_success',$name->product_name.' đã được bỏ khỏi giỏ!');
    }
    public function updateCart(Request $request){
        $data = $request->all();
        $a = $data['data'];
        foreach ($a as $key) {
            //var_dump($value['id']);
            DB::table('cart')->where(['id'=>$key['id']])->update(['quantity'=>$key['qty']]);
        }
        echo "true";
    }
    public function getStockAttribute(Request $request){
        $data = $request->all();
        $get = ProductsAttribute::where(['product_id'=>$data['product_id'],'sku'=> $data['code']])->first();
        $getCart = DB::table('cart')->where(['product_id'=>$data['product_id'],'product_code'=> $data['code']])->first();
        echo $get->stock;
    }  
    public function applyCoupon(Request $request){

        Session::forget('CouponAmount');
        Session::forget('CouponCode');

        $data = $request->all();
        $couponCount = Coupon::where('coupon_code',$data['code'])->count();
        $couponDetails = Coupon::where('coupon_code',$data['code'])->first();
        $current_date = date('Y-m-d');
        $req = "";
        if ($couponCount == 0) {
            return $req = "false";
        }else{
            $expiry_date = $couponDetails->expiry_date;
            if ($couponDetails->status == 0) {
                return $req = "inactive";
            }
            
            if($expiry_date < $current_date){
                return $req = "expiried";
            }
        }

        if (Auth::check()) {
            $user_email = Auth::user()->email;
            if (Session::get('session_id') != null) {
                $userCart = DB::table('cart')->where(['user_email'=>$user_email])->orwhere('session_id',Session::get('session_id'))->get();
            }else{
                $userCart = DB::table('cart')->where(['user_email'=>$user_email])->get();
            }

        }else{
            $session_id = Session::get('session_id');
            $userCart = DB::table('cart')->where(['session_id'=>$session_id])->get();
        }



        $total_amount = 0;
        foreach ($userCart as $item) {
            $total_amount = $total_amount + ($item->price * $item->quantity);
        }
        if ($couponDetails->amount_type == "Fixed") {
            $couponAmount = $couponDetails->amount;
        }
        else{
            $couponAmount = $total_amount * ($couponDetails->amount/100);
        }
        Session::put('CouponAmount',$couponAmount);
        Session::put('CouponCode',$data['code']);
    
    }
    public function clearCart(){
        if (Auth::check()) {
            $user_email = Auth::user()->email;
            if (Session::get('session_id') != null) {
                DB::table('cart')->where(['user_email'=>$user_email])->orwhere('session_id',Session::get('session_id'))->delete();
                return "true";
            }else{
                DB::table('cart')->where(['user_email'=>$user_email])->delete();
                return "true";
            }

        }else{
            $session_id = Session::get('session_id');
            DB::table('cart')->where(['session_id'=>$session_id])->delete();
            return "true";
        }
        
    } 
    public function checkout(Request $request){
        $user_id = Auth::user()->id;
        $user_email = Auth::user()->email;
        $userDetail = User::find($user_id);
        $shippingCount = Deliveries::where('user_id',$user_id)->count();
        $shippingDetails =  array();
        if ($shippingCount>0) {
            $shippingDetails =  Deliveries::where('user_id',$user_id)->first();
        }

        //Cập nhật giỏ hàng với email người dùng
        $session_id = Session::get('session_id');
        DB::table('cart')->where(['session_id'=>$session_id])->update(['user_email'=>$user_email]);
        if ($request->isMethod('post')) {
            $data = $request->all();
            if (isset($data['bill_email'])) {
                return redirect()->back()->with('flash_message_error','Không được thay đổi e-mail tài khoản! Bạn đang can thiệp vào chế độ của nhà phát triển!');
            }else{
                User::where('id',$user_id)->update(['name'=>$data['bill_name'],'phone'=>$data['bill_phone'],'address'=>$data['bill_address']]);
            }
            if ($shippingCount > 0) {
                Deliveries::where('user_id',$user_id)->update(['name'=>$data['ship_name'],'phone'=>$data['ship_phone'],'address'=>$data['ship_address'],'email'=>$data['ship_email']]);
            }else{
                $shipping = new Deliveries;
                $shipping->user_id = $user_id;
                $shipping->user_email = $user_email;
                $shipping->name = $data['ship_name'];
                $shipping->address = $data['ship_address'];
                $shipping->phone = $data['ship_phone'];
                $shipping->email = $data['ship_email'];
                $shipping->save();
            }
            return redirect()->action('ProductsController@orderReview');
        }
        return view('products.checkout')->with(compact('userDetail','shippingDetails'));
    }
    public function orderReview(){
        $user_id = Auth::user()->id;
        $user_email = Auth::user()->email;
        $userDetail = User::find($user_id);
        $shippingCount = Deliveries::where('user_id',$user_id)->count();
        $shippingDetails =  Deliveries::where('user_id',$user_id)->first();
        //var_dump($shippingCount); exit();
        $userCart = DB::table('cart')->where(['user_email'=>$user_email])->get();
        return view('products.order_review')->with(compact('userDetail','shippingDetails','userCart','shippingCount'));
    }
    public function placeOrder(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            //echo "<pre>"; print_r($data); exit();
            if (empty(Session::get('CouponCode'))) {
                $coupon_code = '';
            }else{
                $coupon_code = Session::get('CouponCode');
            }
            if (empty(Session::get('CouponAmount'))) {
                $coupon_amount = 0;
            }else{
                $coupon_amount = Session::get('CouponAmount');
            }
            if (isset($data['email'])) {
                $email = $data['email'];
            }else{
                $email = ' ';
            }
            $user_id = Auth::user()->id;
            $user_email = Auth::user()->email;
            $shippingDetails = Deliveries::where(['user_email'=>$user_email])->first();
            $order = new Orders;
            $order->user_id = $user_id;
            $order->user_email = $email;
            $order->name = $shippingDetails->name;
            $order->address = $shippingDetails->address;
            $order->phone = $shippingDetails->phone;
            $order->coupon_code = $coupon_code;
            $order->coupon_amount = $coupon_amount;
            $order->order_status = '0';
            $order->payment_method = $data['payment'];
            $order->grand_total = $data['grand_total'];
            $order->save();

            $order_id = DB::getPdo()->lastInsertID();
            $cartProducts = DB::table('cart')->where(['user_email'=>$user_email])->get();
            foreach ($cartProducts as $key) {
                $cartPro = new Order_products;
                $cartPro->order_id = $order_id;
                $cartPro->user_id = $user_id;
                $cartPro->product_id = $key->product_id;
                $cartPro->product_code = $key->product_code;
                $cartPro->product_name = $key->product_name;
                $cartPro->product_color = $key->product_color;
                $cartPro->price = $key->price;
                $cartPro->qty = $key->quantity;
                $cartPro->save();
            }
            Session::put('order_id',$order_id);
            Session::put('grand_total',$data['grand_total']);

            //Chuyển đến trang cảm ơn khách hàng
            return redirect('/thanks');
        }
    }
    public function thanks(Request $request){
        $user_email = Auth::user()->email;
        DB::table('cart')->where('user_email',$user_email)->delete();
        return view('products.thanks');
    }
    public function userOrders(){
        $user_id = Auth::user()->id;
        $orders = Orders::with('orders')->where('user_id',$user_id)->get();
        //echo "<pre>"; var_dump($orders); exit();
        return view('orders.users_orders')->with(compact('orders'));
    }
    public function userOrdersDetails($order_id){
        $ordersDetails = Orders::with('orders')->where('id',$order_id)->first();
        return view('orders.users_order_details')->with(compact('ordersDetails'));
    }
    public function viewOrders(){
        $orders = Orders::with('orders')->orderBy('id','Desc')->get();
        return view('admin.orders.view_orders')->with(compact('orders'));
    }
    public function viewOrderDetails($order_id){
        $ordersDetails = Orders::with('orders')->where('id',$order_id)->first();
        $user_id = $ordersDetails->user_id;
        $userDetails = User::where('id',$user_id)->first();
        return view('admin.orders.order_details')->with(compact('ordersDetails','userDetails'));
    }
    public function exportorder(){
        return Excel::download(new OrdersExport, 'don_hang.XLSX');
    }
    public function exportorderDetails($order_id){
        Session::put('order_id',$order_id);
        return Excel::download(new OrderDetailsExport, 'don_hang.XLSX');
    }
    public function updateOrderStatus(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            //var_dump($data); exit();
            if($data['Status']==4){
                Orders::where('id',$data['order_id'])->update(['order_status'=>$data['Status']]);
                if (empty($data['reason'])) {
                    $data['reason'] = '';
                }
                $cancelDetails = Cancels::where('order_id',$data['order_id'])->count();
                if ($cancelDetails < 0) {
                    $cancels = new Cancels;
                    $cancels->order_id = $data['order_id'];
                    $cancels->user_email = $data['order_email'];
                    $cancels->status = 1;
                    $cancels->reason_cancel = $data['reason'];
                    $cancels->save();
                }else{
                    Cancels::where('order_id',$data['order_id'])->update(['reason_cancel'=>$data['reason'],'status'=>1]);
                }
                
                return redirect()->back()->with('flash_message_success','Đơn hàng số '.$data['order_id'].' đã cập nhật thành công');
            }else{
                Orders::where('id',$data['order_id'])->update(['order_status'=>$data['Status']]);
                return redirect()->back()->with('flash_message_success','Đơn hàng số '.$data['order_id'].' đã cập nhật thành công');
            }
        }
    }
    public function cancelDetails(Request $request){
        $data = $request->all();
        $cancels = Cancels::where('order_id',$data['id'])->first();
        return $cancels;
    }
    public function cusomerCancel($order_id){
        $user_email= Auth::user()->email;
        Orders::where('id',$order_id)->update(['order_status'=>4]);
        $cancelDetails = Cancels::where('order_id',$order_id)->count();
        if ($cancelDetails < 0) {
            $cancels = new Cancels;
            $cancels->order_id = $order_id;
            $cancels->user_email = $user_email;
            $cancels->status = 2;
            $cancels->reason_cancel = ' ';
            $cancels->save();
        }else{
            Cancels::where('order_id',$order_id)->update(['status'=>2]);
        }
        return redirect()->back()->with('flash_message_success','Đã huỷ đơn hàng số '.$order_id);
    }
}
