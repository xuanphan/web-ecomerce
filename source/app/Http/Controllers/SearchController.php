<?php

namespace App\Http\Controllers;
use App\Product;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    public function getSearch(Request $req){
    	$data = $req->all();
        $products = Product::where('product_name','like','%'.$data['q'].'%')
                            ->orWhere('price',$data['q'])
                            ->get();
        return view('products.search',compact('products'));                        
    }
}