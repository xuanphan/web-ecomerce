<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Auth;
use App\Orders;

class OrdersExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
    	$user_id = Auth::user()->id;
    	$orders = Orders::with('orders')->where('user_id',$user_id)->where('order_status','!=','4')->get();
        return view('orders.user_orders_report', [
            'orders' => $orders
        ]);
    }
}

