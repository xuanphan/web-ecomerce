<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Orders;
use Session;

class OrderDetailsExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {

         $ordersDetails = Orders::with('orders')->where('id',Session::get('order_id'))->first();
         Session::forget('order_id');
         return view('orders.user_orders_details_report', [
            'ordersDetails' => $ordersDetails
        ]);
    }
}
