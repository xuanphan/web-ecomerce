<?php

use Illuminate\Database\Seeder;

class Productsseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Product::class, 20000)->create();
    }
}
