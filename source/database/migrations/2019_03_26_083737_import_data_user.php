<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Hash;
class ImportDataUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        $pwd = md5('12345678');
        DB::table('admins')->insert([
                'username' => 'admin',
                'password' => $pwd,
                'status' => 1
            ]
        );
        DB::table('banners')->insert([
                'image' => '403.jpg',
                'title' => 'test1',
                'type'=> 1,
                'status' => 1
            ],
            [
                'image' => '403.jpg',
                'title' => 'test2',
                'type'=> 1,
                'status' => 1
            ]

        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
