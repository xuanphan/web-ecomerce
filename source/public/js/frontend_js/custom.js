/*
	Author: Umair Chaudary @ Pixel Art Inc.
	Author URI: http://www.pixelartinc.com/
*/


$(document).ready(function(e) {
    //alert("test");
    // Slideshow
    $("#slishow_wrap12").owlCarousel({
        responsive:{
            0:{
                items:1
            }
        },
        navigation: true,
        dots: true,
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        loop: true
    });
    // ----------AND----------------

    // pproducts_deals_ lefft
    $("#pproducts_deals").owlCarousel({
        responsive:{
            0:{
                items:1
            }
        },
        navigation: true,
        dots: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        loop: true
    });
    // ----------AND----------------

    // sns_blog_ left , testimoniol page2
    $(".slider-left9 ").owlCarousel({
        responsive:{
            0:{
                items:1
            }
        },
        navigation: true,
        dots: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        loop: true
    });
    // ----------AND----------------

    // sns_producttaps1
    $(".taps-slider1").owlCarousel({
        responsive:{
            0:{
                items:1
            }
        },
        navigation: true,
        dots: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        loop: true
    });
    // ----------AND----------------

    // sns_slider1_page2
    $(".sns-slider-page1").owlCarousel({
        responsive:{
            0:{
                items:1
            },
            480:{
                items:2
            },
            768:{
                items:3
            },
            992:{
                items:3
            },
            1200:{
                items:4
            }
        },
        navigation: true,
        dots: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        loop: true
    });
    // ----------AND----------------
    $("#registerform").validate({
        rules:{
            name:{
                required: true,
                minlength: 2,
                accept: "[a-zA-Z]+"
            },
            email:{
                required:true,
                email: true,
                remote: "/check-mail"
            },
            password:{
                required:true,
                minlength:6,
            },
            comfirm_password:{
                required:true,
                minlength:6,
                equalTo:"#myPassword"
            }
        },
        messages:{
            name: {
                required: "Trường này không được để trống! Xin hãy nhập họ tên!",
                minlength: "Họ tên phải nhiều hơn 2 ký tự",
                accept: "Họ tên không được có số"
            },
            password: {
                required: "Xin hãy nhập mật khẩu",
                minlength: "Mật khẩu phải dài hơn 6 ký tự"
            },
            email: {
                required: "Xin hãy nhập Email",
                email: "Email không hợp lệ",
                remote: "Email đã được sử dụng!"
            }
        }
    });
    $("#form_login").validate({
        rules:{
            email:{
                required:true,
                email: true
            },
            password:{
                required:true,
                minlength:6
            }
        },
        messages:{
            email: {
                required: "Xin hãy nhập Email",
                email: "Email không hợp lệ",
            },
            password: {
                required: "Xin hãy nhập mật khẩu"
            }
        }
    });
    $(".user-update").validate({
        rules:{
            name: {
                required: true,
            },
            phone: {
                required: true,
                minlength: 10,
                maxlength: 13,
                number: true
            },
            bitrh_day: {
                required: true,
            },
            sex: {
                required: true,
            }
        },
        messages:{
            name:{
                required: 'Họ tên không được để trống!'
            },
            phone:{
                required: 'Vui lòng cung cấp số điện thoại...',
                minlength: 'Số điện thoại không hợp lệ...',
                maxlength: 'Số điện thoại không hợp lệ...',
                number: 'Số điện thoại không hợp lệ...'
            },
            bitrh_day:{
                required: 'Vui lòng cung cấp ngày sinh...'
            },
            sex:{
                required: 'Vui lòng chọn giới tính...'
            }
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.input-text').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.input-text').removeClass('error');
            $(element).parents('.input-text').addClass('success');
        }
    });
    $('#myPassword').passtrength({
          minChars: 4,
          passwordToggle: true,
          tooltip: true,
          eyeImg:"/images/frontend_images/eye.svg"
        });
    $("#curent_pwd").on('keyup',function(){
        var curent_pwd = $(this).val();
        $.ajax({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'post',
            url: '/check-user-pwd',
            data: {curent_pwd},
            success: function(dat){
                if (dat == "false") {
                    $("#chkPwd").html("<font color = 'red'> Mật khẩu hiện tại không đúng</font>");
                    //alert(dat);
                }else if(dat == "true"){
                    $("#chkPwd").html("<font color ='blue'>Mật khẩu đúng</font>");
                }
            }
        });
    });
    $(".pass-update").validate({
        rules:{
            curent_pwd:{
                required: true
            },
            new_pwd: {
                required: true,
                minlength:6,
                maxlength:20
            },
            confirm_pwd: {
                required: true,
                minlength:6,
                equalTo:"#myPassword"
            }
        },
        messages: {
            curent_pwd:{
                required: "Xin hãy nhập mật khẩu hiện tại của bạn"
            },
            new_pwd: {
                required: "Xin hãy nhập mật khẩu mới",
                minlength: "Mật khẩu phải dài hơn 6 ký tự"
            },
            confirm_pwd: {
                required: "Nhập lại mật khẩu mới",
            }
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.input-text').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.input-text').removeClass('error');
            $(element).parents('.input-text').addClass('success');
        }
    });
    // MENU
    // jQuery(document).ready(function($){
    $('#menu_offcanvas').SnsAccordion({
        accordion: false,
        expand: false,
        btn_open: '<i class="fa fa-plus"></i>',
        btn_close: '<i class="fa fa-minus"></i>'
    });
    $('#sns_mommenu .btn2.offcanvas').on('click', function(){
        if($('#menu_offcanvas').hasClass('active')){
            $(this).find('.overlay').fadeOut(250);
            $('#menu_offcanvas').removeClass('active');
            $('body').removeClass('show-sidebar');
        } else {
            $('#menu_offcanvas').addClass('active');
            $(this).find('.overlay').fadeIn(250);
            $('body').addClass('show-sidebar');
        }
    });
        // if($('#sns_right').length) {
        //     $('#sns_mommenu .btn2.rightsidebar').css('display', 'inline-block').on('click', function(){
        //         if($('#sns_right').hasClass('active')){
        //             $(this).find('.overlay').fadeOut(250);
        //             $('#sns_right').removeClass('active');
        //             $('body').removeClass('show-sidebar1');
        //         } else {
        //             $('#sns_right').addClass('active');
        //             $(this).find('.overlay').fadeIn(250);
        //             $('body').addClass('show-sidebar1');
        //         }
        //     });
        // }
        if($('#sns_left').length) {
            $('#sns_mommenu .btn2.leftsidebar').css('display', 'inline-block').on('click', function(){
                if($('#sns_left').hasClass('active')){
                    $(this).find('.overlay').fadeOut(250);
                    $('#sns_left').removeClass('active');
                    $('body').removeClass('show-sidebar1');
                } else {
                    $('#sns_left').addClass('active');
                    $(this).find('.overlay').fadeIn();
                    $('body').addClass('show-sidebar1');
                }
            });
        }
    // });
    // ----------AND----------------



    // ----------------sns_producttaps_wraps
    $('#sns_producttaps1 .precar').on('click', function(){
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $( "#sns_producttaps1" ).removeClass( "active" );
            $(this).find('[class*="fa-caret-"]').removeClass('fa-align-justify').addClass('fa-align-justify');
            $('#sns_producttaps1 .nav-tabs').stop(true, true).slideUp("1400");
        } else {
            $(this).addClass('active');
            $( "#sns_producttaps1" ).addClass( "test" );
            $(this).find('[class*="fa-caret-"]').removeClass('fa-align-justify').addClass('fa-align-justify');
            $('#sns_producttaps1 .nav-tabs').stop(true, true).slideDown("1400");
        }
    });
    // ----------AND----------------

    // ----------------sns_producttaps_wraps
    $('#sns_slider1_page2 .precar').on('click', function(){
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $( "#sns_slider1_page2" ).removeClass( "active" );
            $(this).find('[class*="fa-caret-"]').removeClass('fa-align-justify').addClass('fa-align-justify');
            $('#sns_slider1_page2 .nav-tabs').stop(true, true).slideUp("1400");
        } else {
            $(this).addClass('active');
            $( "#sns_slider1_page2" ).addClass( "test" );
            $(this).find('[class*="fa-caret-"]').removeClass('fa-align-justify').addClass('fa-align-justify');
            $('#sns_slider1_page2 .nav-tabs').stop(true, true).slideDown("1400");
        }
    });
    // ----------AND----------------

    // ----------------sns_producttaps_wraps
    $('#sns_slider2_page2 .precar').on('click', function(){
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $( "#sns_slider2_page2" ).removeClass( "active" );
            $(this).find('[class*="fa-caret-"]').removeClass('fa-align-justify').addClass('fa-align-justify');
            $('#sns_slider2_page2 .nav-tabs').stop(true, true).slideUp("1400");
        } else {
            $(this).addClass('active');
            $( "#sns_slider2_page2" ).addClass( "test" );
            $(this).find('[class*="fa-caret-"]').removeClass('fa-align-justify').addClass('fa-align-justify');
            $('#sns_slider2_page2 .nav-tabs').stop(true, true).slideDown("1400");
        }
    });
    // ----------AND----------------

    // ----------------sns_producttaps_wraps
    $('#sns_slider3_page2 .precar').on('click', function(){
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $( "#sns_slider3_page2" ).removeClass( "active" );
            $(this).find('[class*="fa-caret-"]').removeClass('fa-align-justify').addClass('fa-align-justify');
            $('#sns_slider3_page2 .nav-tabs').stop(true, true).slideUp("1400");
        } else {
            $(this).addClass('active');
            $( "#sns_slider3_page2" ).addClass( "test" );
            $(this).find('[class*="fa-caret-"]').removeClass('fa-align-justify').addClass('fa-align-justify');
            $('#sns_slider3_page2 .nav-tabs').stop(true, true).slideDown("1400");
        }
    });
    // ----------AND----------------

    // ----------------description
    $('#sns_description .detail-none').on('click', function(){
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $( ".description" ).removeClass( "active" );
            $(this).find('[class*="fa-caret-"]').removeClass('fa-align-justify').addClass('fa-align-justify');
            $('.description .nav-tabs').stop(true, true).slideUp("1400");
        } else {
            $(this).addClass('active');
            $( ".description" ).addClass( "test" );
            $(this).find('[class*="fa-caret-"]').removeClass('fa-align-justify').addClass('fa-align-justify');
            $('.description .nav-tabs').stop(true, true).slideDown("1400");
        }
    });
    // ----------AND----------------

    //-----------suggest collection
    $('#sns_suggest12 .fa').on('click', function(){
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $( ".sns_suggest" ).removeClass( "active" );
            $(this).find('[class*="fa-caret-"]').removeClass('fa-align-justify').addClass('fa-align-justify');
            $('.sns_suggest .suggest-content').stop(true, true).slideUp("1400");
        } else {
            $(this).addClass('active');
            $( ".sns_suggest" ).addClass( "test" );
            $(this).find('[class*="fa-caret-"]').removeClass('fa-align-justify').addClass('fa-align-justify');
            $('.sns_suggest .suggest-content').stop(true, true).slideDown("1400");
        }
    });
    // ----------AND----------------

    // Slideshow
    $("#slider123").owlCarousel({
        responsive:{
            0:{
                items:1
            }
        },
        navigation: false,
        dots: true,
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        loop: true
    });
    // ----------AND----------------

     // slider thumbail
    $("#sns_thumbail").owlCarousel({
        responsive:{
            0:{
                items:3
            },
            640:{
                items:4
            },
            768:{
                items:3
            },
            1000:{
                items:4
            }
        },
        navigation: true,
        dots: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        // singleItem: true,
        loop: true
    });
    // ----------AND----------------

     // blog
    $("#latestblog132").owlCarousel({
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:3
            }
        },
        navigation: true,
        dots: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        // singleItem: true,
        loop: true
    });
    // ----------AND----------------

     // sns-products-list
    $("#products_small").owlCarousel({
        responsive:{
            0:{
                items:1
            },
            530:{
                items:2
            },
            600:{
                items:2
            },
            800:{
                items:2
            },
            992:{
                items:3
            },
            1200:{
                items:4
            }
        },
        navigation: true,
        dots: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        loop: true
    });
    // ----------AND----------------

      // block-bestsaler
    $("#products_slider12").owlCarousel({
        responsive:{
             0:{
                items:1
            }
        },
        navigation: true,
        dots: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        loop: true
    });
    // ----------AND----------------

    // parner
    $("#partners_slider1").owlCarousel({
        responsive:{
            0:{
                items:2
            },
            480:{
                items:3
            },
            600:{
                items:4
            },
            980:{
                items:5
            },
            1000:{
                items:6
            }
        },
        navigation: true,
        dots: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        loop: true
    });
    // ----------AND----------------

    // categories
    $(".featured-slider").owlCarousel({
        responsive:{
            0:{
                items:1
            },
            480:{
                items:2
            },
            768:{
                items:3
            },
            992:{
                items:3
            },
            1200:{
                items:4
            }
        },
        navigation: true,
        dots: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        loop: true
    });
    // ----------AND----------------

    // product_index
    $("#product_index, #product_index1, #product_index2").owlCarousel({
        responsive:{
            0:{
                items:1
            },
            480:{
                items:2
            },
            768:{
                items:3
            },
            992:{
                items:4
            },
            1200:{
                items:5
            }
        },
        navigation: true,
        dots: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        loop: true
    });
    // ----------AND----------------

    // related_upsell
    $("#related_upsell").owlCarousel({
        responsive:{
            0:{
                items:1
            },
            480:{
                items:2
            },
            768:{
                items:3
            },
            1200:{
                items:4
            }
        },
        navigation: true,
        dots: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        loop: true
    });
    // ----------AND----------------

    // related_related
    $("#related_upsell1").owlCarousel({
        responsive:{
            0:{
                items:1
            },
            480:{
                items:2
            },
            768:{
                items:3
            },
            1200:{
                items:4
            }
        },
        navigation: true,
        dots: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        loop: true
    });
    // ----------AND----------------
});
//Thay đổi giá và tình trạng theo màu
$(document).ready(function(e) {
    //alert("test");
    $("#attribute").change(function(){
        var id = $(this).val();
        if(id == ""){
            return false;
        }
        // alert(id);
        $.ajax({
            type: 'get',
            url: '/get-product-price',
            data: {id:id},
            success: function(data){
                //alert(data);
                var arr = data.split('#');
                price_format = arr[0].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
                $("#getPrice").html(price_format+" VNĐ");
                $("#price_att").val(arr[0]);
                if(arr[1]==0){
                    $("#Cart-btn").hide();
                    $("#Availability").text("Hết hàng");
                }else{
                    $("#Cart-btn").show();
                    $("#Availability").text("Còn hàng");
                }
            },error: function(){
                alert("Error");
            }
        });
    });

    $('#form_login').submit(function(ev){
        var email = $('#user_email').val();
        var password = $('#user_password').val();
        $.ajax({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'post',
            url: '/users-login',
            data: {email,password},
            success: function(data){
                if (data == 'true') {
                    location.reload(false);
                }else{
                     $("#message-warning").html('<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert">×</button><strong>Email đăng nhập hoặc password không đúng!</strong></div>');
                }
            }
        });
        ev.preventDefault();
    });

    $(".changeImg").click(function(){
        var image = $(this).attr('id');
        $(".mainImg").attr("src",image);
    });

    $(".quantityPro").change(function(){
        
        var id = $(this).attr('id');
        var QTY_value = parseInt($('#'+id).val());
        var price1 = $('#price1-'+id).val();
        var code = $('#product_code-'+id).val();
        var product_id = $('#product_id-'+id).val();
        var name = $("#product_name-"+id).val();
        if (QTY_value < 1){
            $("#"+id).val(1);
            d = price1.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
            $('#price2-'+id).text(d+' VNĐ');

        }else{
             $.ajax({
            type: 'get',
            url: '/get-stock-attribute',
            data: {code,product_id},
            success: function(data){
                da = parseInt(data);
                if (QTY_value > da) {
                    $("#"+id).css("border-color", "red");
                    $("#message-update").html('<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert">×</button><strong>Hiện tại chỉ còn '+da+' sản phẩm '+name+'!</strong></div>');
                    $("#"+id).val(da);
                    amax =  price1 * da;
                    c = amax.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                    $('#price2-'+id).text(c+' VNĐ');
                }else{
                    $("#"+id).css("border-color", "");
                }
            }
        });
        sum = price1 * QTY_value;
        a = sum.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
        $('#price2-'+id).text(a+' VNĐ');
        }
        
    });
    $("#update-cart").click(function(){
        var data = [];
        $('.rowCart').each(function(index, items){
            
           
            var onerow = {
                id: ($(items).find('.id-cart').val()),
                qty: ($(items).find('.quantityPro').val())
            };
            data.push(onerow);
        });
        //console.log(data);
        $.ajax({
            type: 'get',
            url: '/update-cart/',
            data: {data},
            success: function(data){
                if (data == "true") {
                    setTimeout(function(){ window.location.replace("/cart"); }, 1000);
                    $("#message-update").html('<div class="alert alert-success alert-block"><button type="button" class="close" data-dismiss="alert">×</button><strong>Giỏ hàng đang cập nhật...</strong></div>');
                }
            }
        });
    });
    $("#clear-cart").click(function(){
        $.ajax({
            type: 'get',
            url: '/clear-cart',
            success: function(session){
                if(session == "true"){
                    setTimeout(function(){ window.location.replace("/cart"); }, 1000);
                    $("#message-update").html('<div class="alert alert-success alert-block"><button type="button" class="close" data-dismiss="alert">×</button><strong>Đang tiến hành xóa hàng trong giỏ...</strong></div>');
                }
            }
        });

    });
    $("#Apply_coupon").click(function(){
        var code = $("#coupon_Code").val();
        $.ajax({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'post',
            url: '/cart/apply-coupon',
            data: {code: code},
            success: function(data){
                if (data == "false") {
                    $('.formbd2').css("color","red");
                    $('.formbd2').text('Mã không tồn tại! Xin hãy nhập mã khác!');
                    setTimeout(function(){ window.location.replace("/cart"); }, 1000);
                }else if (data == "expiried") {
                    $('.formbd2').css("color","red");
                    $('.formbd2').text('Mã đã hết hạn! Xin hãy nhập mã khác!');
                    setTimeout(function(){ window.location.replace("/cart"); }, 1000);
                }else if (data == "inactive") {
                    $('.formbd2').css("color","red");
                    $('.formbd2').text('Mã đã ngừng hoạt động! Xin hãy nhập mã khác!');
                    setTimeout(function(){ window.location.replace("/cart"); }, 1000);
                }else{
                    // a = data.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                    // alert(a);
                    $('.formbd2').css("color","green");
                    $('.formbd2').text('Nhập mã thành công!');
                    setTimeout(function(){ window.location.replace("/cart"); }, 1000);
                    // alert(data);
                    
                }
            } 
        });
    });
    $("#coppy_address").click(function(){
        if (this.checked) {
            $("#ship_name").val($("#bill_name").val());
            $("#ship_address").val($("#bill_address").val());
            $("#ship_email").val($("#bill_email").val());
            $("#ship_phone").val($("#bill_phone").val());
        }else{
            $("#ship_name").val(' ');
            $("#ship_address").val(' ');
            $("#ship_email").val(' ');
            $("#ship_phone").val(' ');
        }
    });
    $('.checkout').validate({
        rules:{
            bill_name: {
                required: true,
            },
            bill_address: {
                required: true,
            },
            bill_email: {
                required: true,
            },
            bill_phone: {
                required: true,
            },
            ship_name: {
                required: true,
            },
            ship_address: {
                required: true,
            },
            ship_email: {
                required: true,
            },
            ship_phone: {
                required: true,
            }
        },
         messages: {
            bill_name: {
                required: 'Vui lòng cung cấp thông tin...',
            },
            bill_address: {
                required: 'Vui lòng cung cấp thông tin...',
            },
            bill_email: {
                required: 'Vui lòng cung cấp thông tin...',
            },
            bill_phone: {
                required: 'Vui lòng cung cấp thông tin...',
            },
            ship_name: {
                required: 'Vui lòng cung cấp thông tin...',
            },
            ship_address: {
                required: 'Vui lòng cung cấp thông tin...',
            },
            ship_email: {
                required: 'Vui lòng cung cấp thông tin...',
            },
            ship_phone: {
                required: 'Vui lòng cung cấp thông tin...',
            }
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.input-text').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.input-text').removeClass('error');
            $(element).parents('.input-text').addClass('success');
        }
    });
    
});
function selectPaymentMethod(){
    if ($('#Paypal').is(':checked') || $('#COD').is(':checked')) {
        
    }else{
        $('#message-require').html("<font color='red'>Vui lòng chọn phương thức thanh toán...!</font>");
        return false;
    }
    
}
