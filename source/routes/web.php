<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Auth::routes(['verify'=> true]);

Route::get('/','ShopController@index');

Route::get('/shop-page','ShopController@products');

Route::match(['get','post'],'/admin','AdminController@login');

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');
//Danh sách theo categoriess
Route::get('/products/{url}','ProductsController@products');

//-------------------------------------------

//Trang chi tiết sản phẩm
Route::get('/product/{id}','ProductsController@product');

//-------------------------------------------

//Lấy giá sản phẩm theo màu/size
Route::get('/get-product-price','ProductsController@getProductPrice');
//------------------------------------------

//Thêm vào giỏ hàng
Route::match(['get','post'],'/add-cart','ProductsController@addtocart');
//----------------------

//Giỏ hàng
Route::match(['get','post'],'/cart','ProductsController@cart');
//---------------------------------------
//Xoá sản phảm trong giỏ hàng
Route::get('/cart/delete-product/{id}','ProductsController@deleteCartProduct');
//---------------

//
Route::get('/get-stock-attribute','ProductsController@getStockAttribute');
//--------------

//Update vào giỏ hàng
Route::match(['get','post'],'/update-cart/','ProductsController@updateCart');
//----------------------

//Xác nhận phiếu mua hàng
Route::post('/cart/apply-coupon','ProductsController@applyCoupon');
//--------------------------

//Đăng ký
Route::match(['get','post'],'/users-register','UsersController@register');
//---------------------------

//Đăng nhập
Route::post('/users-login','UsersController@login');
//---------------------------

//Check email đã được sử dụng chưa
Route::match(['get','post'],'/check-mail','UsersController@checkMail');
//----------------------------

//Check email đã được sử dụng chưa
Route::get('/users-logout','UsersController@logout');
//----------------------------

//Tìm kiếm
Route::get('/seach','ShopController@getSearch');
//-----------------------------

//Clear gio hang
Route::get('/clear-cart','ProductsController@clearCart');
//----------------------------

//Khoá đăng nhập người dùng
Route::group(['middleware'=>['frontlogin']],function(){
//Trang thông tin người dùng
	Route::get('/account','UsersController@account');
	//----------------------------

	//Trang cập nhật thông tin người dùng
	Route::match(['get','post'],'/update-account','UsersController@update_user');
	//--------------------------

	//Trang đổi mật khẩu
	Route::match(['get','post'],'/update-password','UsersController@update_pass');
	//--------------------------

	//Kiểm tra mật khẩu người dùng
	Route::post('/check-user-pwd','UsersController@checkpwd');
	//--------------------------

	//Trang xác minh thanh toán
	Route::match(['get','post'],'/checkout','ProductsController@checkout');
	//--------------------------

	//Đơn hàng
	Route::match(['get','post'],'/order-review','ProductsController@orderReview');
	//--------------------------

	//Đặt hàng
	Route::match(['get','post'],'/place-order','ProductsController@placeOrder');
	//--------------------------

	//Lời cảm ơn khách hàng
	Route::get('/thanks','ProductsController@thanks');
	//-------------------------

	//Hoá đơn
	Route::get('/orders','ProductsController@userOrders');
	//--------------------------

	//Hoá đơn sản phẩm
	Route::get('/orders/{id}','ProductsController@userOrdersDetails');
	//--------------------------

	Route::get('/get-order','ProductsController@exportorder');

	Route::get('/get-order-detail/{id}','ProductsController@exportorderDetails');

	Route::get('/customer-cancel/{id}','ProductsController@cusomerCancel');
});
//Trang đăng nhập admin
Route::match(['get','post'],'/admin','AdminController@login');

Route::group(['middleware'=>['adminlogin']],function(){

	Route::get('/admin/dashboard','AdminController@dashboard');
	Route::get('/admin/settings','AdminController@settings');
	Route::get('/admin/check-pwd','AdminController@chkPassword');
	Route::match(['get','post'],'/admin/update-pwd','AdminController@updatePassword');

	//Category của Admin
	Route::match(['get','post'],'/admin/add-category','CategoryController@addCategory');
	Route::match(['get','post'],'admin/edit-category/{id}','CategoryController@editCategory');
	Route::match(['get','post'],'admin/delete-category/{id}','CategoryController@deleteCategory');
	Route::get('/admin/view-categories','CategoryController@viewCategories');

	//Product của Admin
	Route::match(['get','post'],'/admin/add-product','ProductsController@addProduct');
	Route::match(['get','post'],'admin/edit-product/{id}','ProductsController@editProduct');
	Route::match(['get','post'],'admin/delete-product-image/{id}','ProductsController@deleteProductImage');
	Route::match(['get','post'],'admin/delete-product/{id}','ProductsController@deleteProduct');
	Route::get('/admin/view-products','ProductsController@viewProducts');

	//thuộc tính sản phẩm
	Route::match(['get','post'],'admin/add-attribute/{id}','ProductsController@addAttributes');
	Route::match(['get','post'],'admin/update-attribute/{id}','ProductsController@updateAttributes');
	Route::match(['get','post'],'admin/delete-attribute/{id}','ProductsController@deleteAttributes');

	//Phiếu mua hàng
	Route::match(['get','post'],'/admin/add-coupon','CouponsController@addCoupon');
	Route::get('admin/view-coupons','CouponsController@viewCoupons');
	Route::match(['get','post'],'admin/update-coupon/{id}','CouponsController@updateCoupon');
	Route::get('/admin/delete-coupon/{id}','CouponsController@deleteCoupon');

	//Admin Banners(slide) 
	Route::match(['get','post'],'/admin/add-banner','BannersController@addBanner');
	Route::get('admin/view-banners','BannersController@viewBanners');
	Route::match(['get','post'],'/admin/update-banner/','BannersController@updateBanner');
	Route::get('admin/delete-banner/{id}','BannersController@deleteBanner');
	Route::match(['get','post'],'/admin/banner-advertisement','BannersController@bannerindex');

	//Admin Orders Routes
	Route::get('/admin/view-orders','ProductsController@viewOrders');
	Route::get('/admin/view-order/{id}','ProductsController@viewOrderDetails');
	Route::post('/admin/update-order-status','ProductsController@updateOrderStatus');
	Route::get('/cancel-detail','ProductsController@cancelDetails');
	
});

Route::get('/logout', 'AdminController@logout');






